import requests
import jinja2
import pprint
import csv
from decimal import *
import logging
import os
from datetime import datetime
import time
import json
from tm_functions import get_rates_for_specific_currencies, load_xml, writeFile, getFilesFromSource, getPersonDataFromFiles

logger = logging.getLogger('dividends')

logger.setLevel(logging.INFO)

logFolder = 'log'

ENV1 = jinja2.Environment(loader=jinja2.FileSystemLoader('taxmanapp/templates'),
                          trim_blocks=True, lstrip_blocks=True)

def datetimeformat(value, format='%Y-%m-%d'):
    return value.strftime(format)

ENV1.filters['datetimeformat'] = datetimeformat

class Dividend():
        def __init__(self, company_name, dividend_date, dividend_amount, dividend_currency) -> None:
            self.company_name = company_name
            self.dividend_date = dividend_date
            self.div_date_obj = datetime.strptime(self.dividend_date, "%Y-%m-%d")
            self.dividend_amount = Decimal(dividend_amount.replace(',','.'))
            self.dividend_currency = dividend_currency

            self.company_address = ""
            self.company_country = ""
            self.dividend_amount_eur = None

            if self.company_name == '':
                raise Exception
            
        def get_dividend_amount_eur_formatted(self):
            return '{:.2f}'.format(self.dividend_amount_eur)
        
        def get_dividend_date_formatted(self):
            return self.div_date_obj.strftime("%d.%m.%Y")

        def output_compatible_csv(self):

            row = [self.div_date_obj.strftime("%d.%m.%Y"),
                   None,
                   None,
                   self.company_name,
                   self.company_address,
                   self.company_country,
                   '1',
                   '{:.4f}'.format(self.dividend_amount_eur),
                   '0',
                   self.company_country,
                   None]

            return row

def get_address(domain):
    url = "https://company5.p.rapidapi.com/companies/find/stream"

    querystring = {"domain":domain}

    headers = {
        "Authorization": "6nfSsjg6xoGJJkiQMPtRxg:4gI4IHofiL7GjvnE1UPS1b",
        "X-RapidAPI-Key": "Uu55xQlURcmshOBYZhVbZvHZjCt1p1oqEXzjsnuq7sRX2ObcZm",
        "X-RapidAPI-Host": "company5.p.rapidapi.com"
    }

    logger.info('Requesting address for {}'.format(domain))

    response = requests.get(url, headers=headers, params=querystring)

    data = json.loads(response.text)

    '''data = {'aliases': ['Veolia',
             'Veolia Environmental Solutions',
             'Veolia Environmental SA',
             'Veolia Environmental',
             'Veolia Near & Middle East'],
 'category': {'industry': 'Support Services',
              'industryGroup': 'Industrial Goods & Services',
              'naicsCode': '54162',
              'sector': 'Industrials',
              'subIndustry': 'Waste & Disposal Services'},
 'crunchbase': {'handle': 'organization/veolia-environmental-solutions'},
 'description': 'Veolia Environnement SA designs and provides water, waste, '
                'and energy management solutions worldwide. The company is '
                'involved in the resource management, production, and delivery '
                'of drinking water and industrial process water; collection, '
                'treatment, and recycling of wastewater; and design and '
                'construction of treatment and network infrastructure. It also '
                'provides waste collection, waste material recovery, '
                'waste-to-energy, organic waste material recovery, hazardous '
                'waste treatment, dismantling and remediation, urban cleaning, '
                'and industrial maintenance and cleaning services. In '
                'addition, the company engages in the operation and '
                'maintenance of heating and cooling networks; development of '
                'energy services to reduce the energy consumption and CO2 '
                'emissions of buildings; optimization of industrial utilities, '
                'such as steam generation, cooling, electricity, compressed '
                'air; and energy use related to processes and industrial '
                'buildings, as well as produces electricity from biomass. The '
                'company was formerly known as Vivendi Environnement and '
                'changed its name to Veolia Environnement SA in 2003. Veolia '
                'Environnement SA was founded in 1853 and is based in '
                'Aubervilliers, France.',
 'domain': 'veolia.com',
 'domainAliases': ['sarpi.fr', 'veolia.com.au'],
 'emailProvider': False,
 'facebook': {'handle': 'veolia-environnement'},
 'foundedYear': 1853,
 'geo': {'city': 'Aubervilliers',
         'country': 'France',
         'countryCode': 'FR',
         'lat': None,
         'lng': None,
         'postalCode': '93300',
         'state': 'Île-de-France',
         'stateCode': 'IDF',
         'streetName': 'Rue Madeleine-Vionnet',
         'streetNumber': '30',
         'subPremise': None},
 'id': '43f76a1a-dd07-4a46-be40-29e4cf3ecdd6',
 'indexedAt': '2023-04-20T01:01:44.964Z',
 'legalName': 'Veolia Environnement SA',
 'linkedin': {'handle': 'company/veolia-near-middle-east',
              'industry': 'environmental services'},
 'location': '30 Rue Madeleine-Vionnet Aubervilliers, IDF 93300 France',
 'logo': 'http://logo.bigpicture.io/logo/veolia.com',
 'metrics': {'alexaGlobalRank': 47933,
             'alexaUsRank': 139441,
             'annualRevenue': 42885300224,
             'employees': 188662,
             'employeesRange': '10K+',
             'estimatedAnnualRevenue': '$10B+',
             'fiscalYearEnd': None,
             'marketCap': 20545259520,
             'raised': None,
             'trancoRank': 46542},
 'name': 'Veolia Environnement',
 'phone': None,
 'tags': ['Developer Platform',
          'Energy Management',
          'Innovation Management',
          'Management Information Systems',
          'Service Industry',
          'Waste Management',
          'B2B',
          'B2G'],
 'tech': ['kronos',
          'condeco',
          'service_max',
          'oracle_learning',
          'kronos_workforce_central',
          'sap_success_factors_compensation',
          'pardot',
          'quick_base',
          'rd_station',
          'salesforce_crm',
          'force_management',
          'hub_spot_sales_hub',
          'cision',
          'bing_ads',
          'agile_crm',
          'adobe_marketing_cloud',
          'facebook_for_business',
          'oracle_soa',
          'okta',
          'sophos',
          'flowmon',
          'one_login',
          'cisco_asa',
          'azure_active_directory',
          'roll',
          'slack',
          'kayako',
          'smartsheet',
          'microsoft_powerpoint',
          'microsoft_share_point',
          'oracle_9_i',
          'azure_sql',
          'dell_boomi',
          'jira_software',
          'symantec_altiris',
          'amazon_event_bridge',
          'mitel',
          'g_suite',
          'genesys',
          'blue_jeans',
          'microsoft_office_365',
          'j_unit',
          'selenium',
          'tricentis',
          'tricentis_tosca',
          'infoblox',
          'citrix_adc',
          'avaya_routers',
          'ibm_mainframe',
          'cisco_cgs_2500',
          'cisco_nexus_switches',
          'ansys',
          'harvest',
          'any_logic',
          'adobe_in_design',
          'bentley_auto_plant',
          'oracle_primavera_suite',
          'camtasia',
          'freshdesk',
          'freshservice',
          'salesforce_desk_com',
          'ui_path',
          'sage_300_cloud',
          'automation_anywhere',
          'sage_business_cloud',
          'infor_enterprise_asset',
          'oracle_project_management',
          'v_mware',
          'sap_net_weaver',
          'google_cloud_run',
          'sap_cloud_platform',
          'ibm_z_series_mainframe',
          'azure_analysis_services',
          'drupal',
          'you_tube',
          'aws_code_pipeline',
          'aws_cloud_formation',
          'google_cloud_serverless',
          'clio',
          'xero',
          'basware',
          'sap_erp',
          'quick_books',
          'sap_erp_central_component_ecc',
          'aconex',
          'promas',
          'bentley',
          'oracle_consulting',
          'sap_ariba_sourcing',
          'autodesk_navisworks',
          'hotjar',
          'logikcull',
          'micro_strategy',
          'tibco_jaspersoft',
          'tableau_software',
          'azure_machine_learning',
          'vb_net',
          'java_script',
          'code_igniter',
          'microsoft_azure',
          'oracle_database',
          'google_dialogflow',
          'agilent',
          'factory_talk'],
 'ticker': 'VIE.PA',
 'twitter': {'avatar': 'https://pbs.twimg.com/profile_images/668049310655889408/PL9XfBUr_normal.png',
             'bio': 'Veolia is the benchmark company for '
                    '#EcologicalTransformation.\n'
                    'Water 💧 Waste ♻️ Energy ⚡️ We are #ResourcingTheWorld 🌍',
             'followers': 37342,
             'following': 1686,
             'handle': 'Veolia',
             'id': '95687779',
             'location': 'Paris, France',
             'site': 'http://www.veolia.com'},
 'type': 'public',
 'url': 'https://www.veolia.com/fr',
 'utcOffset': None}'''

    time.sleep(0.02)

    return data

def read_dividends_to_lookup(file_path, year):

    dividends = []

    with open(file_path, 'r', encoding='utf-8-sig') as file:
        csvreader = csv.DictReader(file, delimiter=';')
        for row in csvreader:
            this_dividend = Dividend(row['company name'], row['dividend date'], row['dividend amount'], row['dividend currency'])
            this_year = this_dividend.dividend_date[:4]
            if this_year != year:
                continue

            dividends.append(this_dividend)

    return dividends

def read_domains():
    # expecting file with headers
    # company
    # domain

    domains = {}

    with open("dividend_lookup/name_to_domain.csv", 'r') as file:
        csvreader = csv.DictReader(file, delimiter=';')
        for row in csvreader:
            domains[row['company']] = row['domain']

    return domains

class company_with_geo_data():
    def __init__(self, name) -> None:
        self.name = name
        self.domain = None
        self.address = None
        self.country = None

def read_addresses():
    # expecting file with headers
    # company
    # address
    # country
    addresses = {}

    with open("dividend_lookup/name_to_address.csv", 'r') as file:
        csvreader = csv.DictReader(file, delimiter=';')
        for row in csvreader:
            addresses[row['company']] = company_with_geo_data(row['company'])
            addresses[row['company']].address = row['address']
            addresses[row['company']].country = row['country']

    return addresses

def lookup_address_data(domain):

    logger.info('Getting address from external source for {}'.format(domain))
    
    lookup_result = get_address(domain)

    company_address = "Unknown Address"
    company_country = "Unknown Country"

    if 'geo' in lookup_result: # lookup was successful

        company_address = '{}, {}, {} {}'.format(lookup_result['geo']['streetName'],
                                                    lookup_result['geo']['streetNumber'],
                                                    lookup_result['geo']['postalCode'],
                                                    lookup_result['geo']['city'])

        company_country = lookup_result['geo']['countryCode']

    return (company_address, company_country)

def process_dividends(source_folder, year):

    filesInSourceFolder = getFilesFromSource(source_folder)
    person_data = getPersonDataFromFiles(filesInSourceFolder)

    dividends = read_dividends_to_lookup(os.path.join(source_folder, filesInSourceFolder[0]), year)

    domains = read_domains()

    addresses = read_addresses()

    dict_valut = load_xml()

    # Get names of all companies we need to get addresses for

    companies_from_this_run = {}

    logger.info('Getting names of all companies we need to get addresses for.')

    for div in dividends:
        if div.company_name not in companies_from_this_run:
            companies_from_this_run[div.company_name] = company_with_geo_data(div.company_name)

    logger.info('{} distinct companies found.'.format(len(companies_from_this_run)))

    # How many already have the right address

    for company in companies_from_this_run:
        count = 0
        if company in addresses:
            count += 1
            companies_from_this_run[company].address = addresses[company].address
            companies_from_this_run[company].country = addresses[company].country

        logger.info('For {} companies, we already have address data in archive.'.format(count))
 
    # For those we don't, do we have the domain? If not, abort

    for company in companies_from_this_run:
        if not companies_from_this_run[company].address:
            if company not in domains:
                raise Exception('Company {} does not yet have a known address nor a domain name.'.format(company))
            else:
                companies_from_this_run[company].domain = domains[company]
 
    # Look up addresses
    for company in companies_from_this_run:
        if companies_from_this_run[company].domain:
            lookup_result = lookup_address_data(companies_from_this_run[company].domain)
            companies_from_this_run[company].address = lookup_result[0]
            companies_from_this_run[company].country = lookup_result[1]

    # copying addresses to dividends

    for div in dividends:
        div.company_address = companies_from_this_run[div.company_name].address
        div.company_country = companies_from_this_run[div.company_name].country

    # converting amount to eur, first get appropriate dates

    dates_and_currencies = []

    for div in dividends:
        dates_and_currencies.append((div.div_date_obj, div.dividend_currency))

    rates = get_rates_for_specific_currencies(dates_and_currencies, dict_valut)

    # now add eur value to each eur

    for div in dividends:
        div.dividend_amount_eur = div.dividend_amount / rates[(div.div_date_obj.date(), div.dividend_currency)]

    # adding out new addresses
    for div in dividends:
        if div.company_address:
            if div.company_name not in addresses:
                addresses[div.company_name] = company_with_geo_data(div.company_name)
            addresses[div.company_name].address = div.company_address
            addresses[div.company_name].country = div.company_country

    # writing out obtained addresses

    with open('dividend_lookup/name_to_address.csv', mode='w') as out_file:
        div_writer = csv.writer(out_file, delimiter=';')
        div_writer.writerow(['company','address','country'])

        for item in addresses:
            div_writer.writerow([item, addresses[item].address, addresses[item].country])

    # writing out xml template

    div_template = ENV1.get_template('template_div.xml')
    div_template_rendered = div_template.render(
        dividends=dividends,
        person_data=person_data,
        year=year
    )
    writeFile(
        "generated",
        div_template_rendered,
        person_data['tax_number'],
        person_data['name'],
        output_type='div')
            
# def write_dividends(source_folder, year, dividends):
#     with open('generated/{}_{}/e_davki_div.csv'.format(source_folder, year), mode='w') as out_file:
#         div_writer = csv.writer(out_file)
#         div_writer.writerow(['#FormCode','Version','TaxPayerID','TaxPayerType','DocumentWorkflowID'])
#         div_writer.writerow(['DOH-DIV','3.9','','FO','O'])
#         div_writer.writerow(['#datum prejema dividende','davčna številka izplačevalca dividend','identifikacijska  številka izplačevalca dividend','naziv izplačevalca dividend','naslov izplačevalca dividend','država izplačevalca dividend','vrsta dividende','znesek dividend','tuji davek','država vira','uveljavljam oprostitev po mednarodni pogodbi'])

#         for div in dividends:
#             div_writer.writerow(div.output_compatible_csv())

