# Installation

1. Clone git project.
1. Create virtual environment in source folder. Requirements: Python3, jinja2, xlrd.
1. Create folders log, generated, source
1. Download latest file dtecbs-l.xml from Bank of Slovenia website and place it in exchange_rate folder.
2. Delete file xml_rates.p to refresh exchange rates
3. Check absolute paths for variables APP_SOURCE_FOLDER and APP_OUTPUT_FOLDER in taxman.py


# Usage

1. Place all source files for a person in a folder, then place that in folder source.
2. Name files according to the convention {tax_number} #{Name and surname}# anything {file type} anything.csv
3. run `python taxman.py`.

# Notes regarding input data

- All trades from future years will be discarded.

- All trades that are closed in past years, will be discarded.

Input data should be prepared in one of two ways:

- all trading history is present, from trade 0 onwards
- trades from current year are present, and from the past
only trades that create positions closed in current year are included.

After running the script, a residuals file will be created which can be used
as the second type of input for next year.
