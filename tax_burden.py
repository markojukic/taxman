from dateutil.relativedelta import *
import logging

logger = logging.getLogger('taxman')

class Tax_burden():
    def __init__(self):
        self.taxable= {
            'stocks': {
                5: 0.0,
                10: 0.0,
                15: 0.0,
                20: 0.0
                },
            'derivatives': {
                1: 0.0,
                5: 0.0,
                10: 0.0,
                15: 0.0,
                20: 0.0            
            }}
                        
        self.unclaimed_loss = {
            'stocks': 0.0,
            'derivatives': 0.0
            }
        self.tax_rates = {
        'stocks': {
                5: 0.275,
                10: 0.2,
                15: 0.15,
                20: 0.1
            },
        'derivatives': {
                1: 0.4,
                5: 0.275,
                10: 0.2,
                15: 0.15,
                20: 0.1
                }
        }

        self.burden = {'stocks': 0, 'derivatives': 0}	
    
    def distribute_losses(self):
        def is_there_something_to_tax(type):
            if sum(self.taxable[type].values()) > 0:
                return True

        def is_there_loss_to_use_up(type):
            if self.unclaimed_loss[type] > 0:
                return True
        types = ['stocks', 'derivatives']

        for type in types:
            while is_there_something_to_tax(type) and is_there_loss_to_use_up(type):
                for tax_period in self.taxable[type].keys():
                    if self.taxable[type][tax_period] > 0:
                        if self.taxable[type][tax_period] >= self.unclaimed_loss[type]:
                            logging.debug('Tax base {} for {} is larger than unclaimed loss: {}'.format(self.taxable[type][tax_period], tax_period, self.unclaimed_loss[type]))
                          
                            self.taxable[type][tax_period] -= self.unclaimed_loss[type]
                            self.unclaimed_loss[type] = 0
                        
                        else:
                            
                            logging.debug('Tax base for {} is smaller than unclaimed loss: {}'.format(tax_period, self.unclaimed_loss[type]))
                            self.unclaimed_loss[type] -= self.taxable[type][tax_period]
                            self.taxable[type][tax_period] = 0
                        
                        break

    def calculate_burden(self):

        for type in self.burden.keys():
            for rate in self.taxable[type]:
                self.burden[type] += self.taxable[type][rate] * self.tax_rates[type][rate]

if __name__ == '__main__':
    test_base = Tax_burden()
    test_base.taxable['stocks'][5] = 120
    test_base.taxable['stocks'][10] = 60
    test_base.unclaimed_loss['stocks'] = 140
    test_base.distribute_losses()
    assert test_base.taxable['stocks'][5] == 0
    assert test_base.taxable['stocks'][10] == 40
    assert test_base.unclaimed_loss['stocks'] == 0
    print('All tests pass.')
