# -*- coding: utf-8 -*-
import jinja2
from tm_item import Item
from tax_burden import Tax_burden
from tm_functions import load_xml, getPersonDataFromFiles, get_rates_for_specific_currencies, \
    obtain_residuals, obtain_residuals2, does_list_contain_buy_and_sell, load_instrument_currency_registry_etoro, \
    getFilesFromSource, dataToTimelines, writeFile, archiveFile, sanitize_name, askForPersonData
from import_functions import genImporterExcel, readSourceFile, importerResiduals, importerTradeStation, importerEtoro, importerEtoro2025
from dividends import process_dividends
from time import strftime, time
from operator import itemgetter
import pickle
import os
from pprint import pprint
import logging
import re
from xlrd import open_workbook, XL_CELL_DATE, xldate_as_tuple
import sys
from collections import Counter
import datetime

# Nastavitve Jinja

ENV1 = jinja2.Environment(loader=jinja2.FileSystemLoader('taxmanapp/templates'),
                          trim_blocks=True, lstrip_blocks=True)


def datetimeformat(value, format='%Y-%m-%d'):
    return value.strftime(format)


ENV1.filters['datetimeformat'] = datetimeformat

# Lokacija mape za uvoz
APP_SOURCE_FOLDER = "source"
APP_OUTPUT_FOLDER = "generated"
APP_OUTPUT_RESIDUALS_FOLDER = "residuals"
APP_DIVIDENDS_FOLDER = "dividends"

# logging
logFolder = 'log'

# logging settings for this run
logger = logging.getLogger('taxman')
logger.setLevel(logging.DEBUG)

# create a log folder if it does not exist

if not os.path.exists(logFolder):
    os.makedirs(logFolder)


def process_trading(source_folder, year):
    data = []

    template_ifi_path = 'template_ifi.xml' if int(
        year) > 2019 else 'template_ifi_2019.xml'

    filesInSourceFolder = getFilesFromSource(source_folder)
    person_data = getPersonDataFromFiles(filesInSourceFolder)

    if None in person_data.values():
        askForPersonData()

    logger.info('\nProcessing {} {}\n'.format(
        person_data['name'], person_data['tax_number']))

    etoro_registry = load_instrument_currency_registry_etoro()

    if os.path.exists('etoro_tickers_to_instruments_suggested.txt'):

        os.remove('etoro_tickers_to_instruments_suggested.txt')

    for file in getFilesFromSource(source_folder):

        print("Reading file {}".format((file)))

        filePath = os.path.join(source_folder, file)

        reader = readSourceFile(filePath)

        if reader in (importerEtoro, importerEtoro2025):
            new_instance = reader(filePath, etoro_registry)
        else:
            new_instance = reader(filePath)
        
        data.extend(new_instance.readFile())

    print("*"*40)

    for count, trade_row in enumerate(data):
        trade_row.append(count)

    # sort vrstic po instrumentu, tipu instrumenta, datumu, vrstnemu redu v izvorni datoteki, smeri nakupa #
    source_rows = sorted(data, key=itemgetter(0, 5, 2, 8, 1))

    # xml v dict
    dict_valut = load_xml()

    # pridobitev tečajev za kalkulacije

    tradetimes_and_currencies = [(row[2], row[6]) for row in source_rows]
    dict_of_rates = get_rates_for_specific_currencies(tradetimes_and_currencies, dict_valut)

    timelines = dataToTimelines(source_rows)

    instrumentsArray = []

    for timeline in timelines:
        for trade in timelines[timeline]:  # converting price to EUR
            trade[4] = trade[4]/dict_of_rates[(trade[2].date(), trade[6])]

        instrumentsArray.append(
            Item(sanitize_name(timeline[0]), timeline[1], timeline[2], timeline[3], timelines[timeline]))

    # Preparing total tax base

    tax_base = Tax_burden()

    # Processing instruments

    for instrument in instrumentsArray:
        logger.debug('\n\tProcessing new instrument: {}'.format(instrument))
        instrument.removeTradesFromFutureYears(year)
        instrument.addStockToTradeObjects()
        instrument.splitTradesThatSwitchPositions()
        instrument.populateLongShortLists()
        instrument.removeTradesFromPreviousYears(year)
        instrument.moveResidualTrades()
        instrument.addStockToTradeObjectsDict()
        instrument.finalize_values_for_output()
        instrument.make_profit_loss_pairs()
        instrument.calculate_tax_burden()

        existing_tax_base = Counter(tax_base.taxable[instrument.group])
        ins_tax_base = Counter(instrument.taxable)

        logger.debug('{} adds {} to tax burden.'.format(
            instrument.ticker,
            ' ,'.join(['{} - {}'.format(item, instrument.taxable[item]) for item in instrument.taxable])))

        tax_base.taxable[instrument.group] = dict(
            existing_tax_base + ins_tax_base)
        tax_base.unclaimed_loss[instrument.group] += instrument.unclaimed_loss

        logger.debug('{} adds {} to unclaimed losses, now at {}.'.format(
            instrument.ticker,
            instrument.unclaimed_loss,
            tax_base.unclaimed_loss))
    # Calculate tax base

    logger.debug('\nCalculating tax burden now.')
    logger.debug('Before applying losses: {}.'.format(tax_base.taxable))
    tax_base.distribute_losses()
    logger.debug('After applying losses: {}.'.format(tax_base.taxable))
    tax_base.calculate_burden()

    msg = 'Tax burden for {}:\n\tStocks: {:.2f} EUR\n\tDerivatives: {:.2f} EUR'.format(
        year,
        tax_base.burden['stocks'],
        tax_base.burden['derivatives'],
    )
    logger.info(msg)
    print(msg)

    # Deduplicate tickers
    tickers = [
        instrument.ticker for instrument in instrumentsArray if instrument.type == '00']

    # count duplicate names and occurences
    tickers_counted = {}

    for ticker in tickers:
        if ticker not in tickers_counted:
            tickers_counted[ticker] = 0
        tickers_counted[ticker] += 1

    tickers_to_deduplicate = [
        ticker for ticker in tickers_counted if tickers_counted[ticker] > 1]
    # iterate

    for ticker in tickers_to_deduplicate:
        logger.debug('Deduplicating {}'.format(ticker))
        for i in range(tickers_counted[ticker]):
            for instrument in [instrument for instrument in instrumentsArray if instrument.type == '00']:
                if instrument.ticker == ticker:
                    instrument.ticker = '{}{:2d}'.format(
                        instrument.ticker[::-1][:-2], i)
                    instrument.fauxisin = '{}{:2d}'.format(
                        instrument.fauxisin[::-1][:-2], i)
                    logger.debug(
                        'New instrument name now {}'.format(instrument.ticker))
                    # changing the list while iterating over it is not a good idea? Am I not doing this just
                    break

    # IFI generiranje itemov

    itemsWithLongIFI = [instrument for instrument in instrumentsArray
                        if len(instrument.tradeObjectsDict['long']) != 0
                        and instrument.type in ('01', '02', '03', '04')]
    itemsWithShortIFI = [instrument for instrument in instrumentsArray
                         if len(instrument.tradeObjectsDict['short']) != 0
                         and instrument.type in ('01', '02', '03', '04')]

    # KDVP generiranje itemov

    itemsWithLong = [instrument for instrument in instrumentsArray if len(
        instrument.tradeObjectsDict['long']) != 0 and instrument.type == '00']
    itemsWithShort = [instrument for instrument in instrumentsArray if len(
        instrument.tradeObjectsDict['short']) != 0 and instrument.type == '00']

    # Čiščenje output folderja

    if len(itemsWithLongIFI) + len(itemsWithShortIFI) + len(itemsWithLong) + len(itemsWithShort) != 0:
        archiveFile(
            APP_OUTPUT_FOLDER,
            person_data['tax_number']
        )

    # Output residuals
    residualsTemplate = \
        "instrument,buysell,datum,čas,količina,cena,tip instrumenta,valuta instrumenta,multiplikator\n"
    for instrument in instrumentsArray:
        residualsTemplate += instrument.getResidualsAsString()

    writeFile(APP_OUTPUT_FOLDER,
              residualsTemplate,
              person_data['tax_number'],
              person_data['name'],
              output_type='residuals')

    writeFile(APP_OUTPUT_RESIDUALS_FOLDER,
              residualsTemplate,
              person_data['tax_number'],
              person_data['name'],
              output_type='residuals',
              is_residual=True,
              year=year)

    logger.info('Ustvarjena datoteka tipa residuals. \
	Stevilo odvecnih poslov: {:d}.'.format(-2 + len(residualsTemplate.split('\n'))))

    # Pisanje IFI

    if len(itemsWithLongIFI) + len(itemsWithShortIFI) != 0:

        ifiTemplate = ENV1.get_template(template_ifi_path)
        ifiTemplateRendered = ifiTemplate.render(
            titems_long=itemsWithLongIFI,
            titems_short=itemsWithShortIFI,
            person_data=person_data,
            year=year
        )

        writeFile(
            APP_OUTPUT_FOLDER,
            ifiTemplateRendered,
            person_data['tax_number'],
            person_data['name'],
            output_type='ifi')

        logger.info('Ustvarjena datoteka tipa IFI. Long: {:s}. Short: {:s}.'.format(
            ','.join(item.name for item in itemsWithLongIFI),
            ','.join(item.name for item in itemsWithShortIFI)
        )
        )

    # Pisanje KDVP

    if len(itemsWithLong)+len(itemsWithShort) != 0:

        kdvpTemplate = ENV1.get_template('template_kdvp.xml')
        kdvpTemplateRendered = kdvpTemplate.render(
            kdvpitems_long=itemsWithLong,
            kdvpitems_short=itemsWithShort,
            person_data=person_data,
            year=year
        )

        writeFile(
            APP_OUTPUT_FOLDER,
            kdvpTemplateRendered,
            person_data['tax_number'],
            person_data['name'],
            output_type='kdvp')

        logger.info('Ustvarjena datoteka tipa KDVP. Long: {:s}. Short: {:s}.'.format(
            ','.join(item.name for item in itemsWithLong),
            ','.join(item.name for item in itemsWithShort)
        )
        )

if __name__ == '__main__':
    if len(sys.argv) != 2:
        year = str(datetime.datetime.now().year)
        print('Script needs one argument, year of report. Proceeding with current year ({})'.format(year))

    else:
        year = sys.argv[1]

    assert len(year) == 4
    assert year[:2] == '20'

    logName = os.path.join(logFolder, 'taxman.log')
    # create a file handler
    handler = logging.FileHandler(logName, encoding='utf-8', mode='w')

    # create a logging format
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(handler)

    # processing trading

    for folder in os.listdir(APP_SOURCE_FOLDER):
        folder_path = os.path.join(APP_SOURCE_FOLDER, folder)
        if os.path.isdir(folder_path):
            print('Processing {}'.format(folder_path))
            process_trading(folder_path, year)
        else:
            print("There are no subfolders in the source folder.")
    
    # Processing dividends

    for folder in os.listdir(APP_DIVIDENDS_FOLDER):
        folder_path = os.path.join(APP_DIVIDENDS_FOLDER, folder)
        if os.path.isdir(folder_path):
            print('Processing {}'.format(folder_path))
            process_dividends(folder_path, year)

        else:
            print("There are no subfolders in the source folder.")

        