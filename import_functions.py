## from logging import raiseExceptions
import logging
from xlrd import open_workbook, xldate_as_tuple
from datetime import datetime, time
from collections import OrderedDict
import csv
from decimal import *
from tm_functions import load_xml, getPersonDataFromFiles, get_rates_for_all_currencies
import os

logger = logging.getLogger('taxman')

class HeaderList():
    """
    Container za celo strukturo
    """

    def __init__(self):
        self.headerList = [
            sourceHeader(['instrument', 'symbol']),
            sourceHeader(['buysell', 'b/s'], validationBuySell),
            sourceHeader(['tradetime', 'trade time',
                         'trade time GMT'], validationIsDate),
            sourceHeader(['quantity', 'amount'], validationIsFloatOrInt),
            sourceHeader(['tradeprice', 'price'], validationIsFloatOrInt),
            sourceHeader(['assetclass', 'product'], validationAssetClass),
            sourceHeader(['currencyprimary', 'currency']),
            sourceHeader(['multiplier'], validationIsFloatOrInt)
        ]

    def validateIndexes(self):
        for sourceHeader in self.headerList:
            if sourceHeader.index is None:
                print(sourceHeader.index)
                raise Exception('No header with name {:s}.'.format(
                    ','.join(sourceHeader.headerNames)))
        return True


class sourceHeader():
    """
    Seznam teh classev določa formalizirano strukturo podatkov, kamor mora vsaka polje iti,
    preden gre v Taxmana. Vsebuje seznam možnih headerjev,
    ki jih lahko vidimo v Excelu, in na katero mesto morajo iti,
    kakor tudi validacijsko funkcijo za vsako polje.
    """

    def __init__(self, headerNames, validation=None):
        self.headerNames = headerNames
        self.validation = validation
        self.index = None

    def validate(self, value):
        return self.validation(value) if self.validation else value

    def assignIndex(self, header, index):
        if header.lower() in self.headerNames:
            self.index = index


def validationBuySell(value):
    buysellDict = {'Bought': 'B', 'Sold': 'S', 'B': 'B', 'S': 'S'}
    try:
        rv = buysellDict[value]
    except KeyError:
        raise Exception('Undefined trade direction: {:s}'.format(value))

    return rv


def validationIsDate(value):
    datetuple = xldate_as_tuple(value, 0)

    dateObj = datetime(*datetuple)

    return dateObj


def validationIsFloatOrInt(value):
    assert isinstance(value, float) or isinstance(value, int), '{:s} is not int or float.'\
    .format(value)
    return abs(value)


def validationAssetClass(value):
    assetClassDict = {('shares'): 0, ('futures'): 1,
                       ('cmdty'): 2, ('cfds'): 2, ('opt'): 3}
    try:
        key = [key for key in sorted(
            assetClassDict) if value.lower() in ''.join(key)][0]
    except IndexError:
        raise Exception('Instrument type {:s} unknown.'.format(value))

    return assetClassDict[key]


class genImporterExcel():
    def __init__(self, filepath):
        book = open_workbook(filepath)
        sheet = book.sheet_by_index(0)

        def convertDateOrReturn(value):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same
            """
            try:
                datetuple = xldate_as_tuple(value, 0)
                print(datetime(*datetuple))
                raise Exception
                return datetime(*datetuple)

            except:
                return value

        self.data = []

        for x in range(1, sheet.nrows):
            thisRow = OrderedDict()
            for y in range(sheet.ncols):
                thisRow[sheet.cell(0, y).value] = sheet.cell(x, y).value
            self.data.append(thisRow)

        headerList = HeaderList()

        # imamo vse podatke v dictu. Potrebno iskati prave stolpce, da dobimo mapiranje
        for i, dataHeader in enumerate(self.data[0]):
            for sourceHeader in headerList.headerList:
                sourceHeader.assignIndex(dataHeader, i)

        headerList.validateIndexes()

        source_rows = []
        for row in range(1, sheet.nrows):

            this_row = []

            for header in headerList.headerList:
                value_to_append = header.validate(
                    sheet.cell(row, header.index).value)
                if header.validation:
                    if header.validation.__name__ == "validationIsFloatOrInt":
                        value_to_append = Decimal(value_to_append)
                this_row.append(value_to_append)

            source_rows.append(this_row)

        self.readData = source_rows

    def readFile(self):
        return self.readData


class genImporterSaxo():
    def __init__(self, file_path):
        self.file_path = file_path

    def readFile(self):
        book = open_workbook(self.file_path)
        sheet = book.sheet_by_index(1)

        def convertDateOrReturn(value):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same

            datetuple = xldate_as_tuple(value, 0)
            return datetime(*datetuple)
            """
            if isinstance(value, str):
                try: 
                    date_obj = datetime.strptime(value, '%d.%m.%Y')
                except ValueError:
                    try:
                        date_obj = datetime.strptime(value, '%d-%b-%Y')
                    except ValueError:
                        raise ValueError("Cannot convert date")
            else:
                datetuple = xldate_as_tuple(value, 0)
                date_obj = datetime(*datetuple)

            return date_obj

        self.data = []

        for x in range(1, sheet.nrows):
            thisRow = OrderedDict()
            for y in range(sheet.ncols):
                thisRow[sheet.cell(0, y).value] = sheet.cell(x, y).value
            self.data.append(thisRow)

        headers_eng = {
            'ticker': 'Instrument',
            'buysell': 'B/S',
            'timestamp': 'TradeTime',
            'quantity': 'Amount',
            'price': 'Price',
            'type': 'Asset type',
            'currency': 'Instrument Currency',
            'multiplier': 'Multiplier'
        }

        headers_si = {
            'ticker': 'Instrument',
            'buysell': 'K/P',
            'timestamp': 'Čas posla',
            'quantity': 'Znesek',
            'price': 'Tečaj',
            'type': 'Vrsta sredstva',
            'currency': 'Valuta instrumenta',
            'multiplier': 'Multiplikator'
        }

        asset_classes_dict = {
            'CfdOnStock': 2,
            'CfdOnIndex': 2,
            'CfdOnFutures': 2,
            'Stock': 0,
            'ContractFutures': 1,
        }

        headers = headers_eng if 'Trade ID' in self.data[0].keys(
        ) else headers_si

        source_rows = []

        for row in self.data:
            if row[headers['type']] not in asset_classes_dict.keys():
                continue

            else:
                this_source_row = []
                this_source_row.append(row[headers['ticker']])

                if row[headers['buysell']] == 'Sold':
                    this_source_row.append('S')
                elif row[headers['buysell']] == 'Bought':
                    this_source_row.append('B')
                else:
                    raise Exception("Trade Type is {:s}, which is not 'Bought' or 'Sold'".format(
                        row[headers['buysell']]))

                this_source_row.append(
                    convertDateOrReturn(row[headers['timestamp']]))
                this_source_row.append(abs(Decimal(row[headers['quantity']])))
                this_source_row.append(Decimal(row[headers['price']]))
                this_source_row.append(
                    asset_classes_dict[row[headers['type']]])
                this_source_row.append(row[headers['currency']])

                if headers['multiplier'] in row.keys():
                    multiplier = Decimal(row[headers['multiplier']])
                else:
                    multiplier = Decimal(1)
                this_source_row.append(multiplier)

            source_rows.append(this_source_row)

        return source_rows

class importMetaTrader2021():
    def __init__(self, file_path):
        self.file_path = file_path

    def readFile(self):
        book = open_workbook(self.file_path)
        sheet = book.sheet_by_index(0)

        def convertDateOrReturn(value):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same

            datetuple = xldate_as_tuple(value, 0)
            return datetime(*datetuple)
            """
            if isinstance(value, str):
                date_obj = datetime.strptime(value, '%Y.%m.%d %H:%M:%S')
            else:
                datetuple = xldate_as_tuple(value, 0)
                date_obj = datetime(*datetuple)

            return date_obj

        self.data = []

        sheet_position_in_range_of_trades = False
        row_with_headers_for_trades = 0

        for x in range(1, sheet.nrows):
            if sheet_position_in_range_of_trades:
                if x == row_with_headers_for_trades:
                    continue
                if sheet.cell(x, 0).value == '':
                    sheet_position_in_range_of_trades = False
                else:
                    thisRow = OrderedDict()
                    for y in range(sheet.ncols):
                        thisRow[sheet.cell(row_with_headers_for_trades, y).value] = sheet.cell(x, y).value
                    self.data.append(thisRow)
            
            else:

                if sheet.cell(x,0).value == 'Posli':
                    sheet_position_in_range_of_trades = True
                    row_with_headers_for_trades = x + 1

        headers_si = {
            'ticker': 'Simbol',
            'buysell': 'Tip',
            'timestamp': 'Čas',
            'quantity': 'Obseg',
            'price': 'Cena'
        }

        headers = headers_si # tukaj se lahko naredi preverbo jezika, zaenkrat podprta samo SI

        source_rows = []

        sheet_position_in_range_of_trades = False

        for row in self.data:
            if row[headers['buysell']] not in ('buy', 'sell'):
                continue

            else:
                this_source_row = []
                this_source_row.append(row[headers['ticker']])

                if row[headers['buysell']] == 'sell':
                    this_source_row.append('S')
                elif row[headers['buysell']] == 'buy':
                    this_source_row.append('B')
                else:
                    raise Exception("Trade Type is {:s}, which is not 'Bought' or 'Sold'".format(
                        row[headers['buysell']]))

                this_source_row.append(
                    convertDateOrReturn(row[headers['timestamp']]))
                this_source_row.append(abs(Decimal(row[headers['quantity']])))
                this_source_row.append(Decimal(row[headers['price']]))
                this_source_row.append(2) #CFD
                this_source_row.append('USD')
                this_source_row.append(1) #multiplier

            source_rows.append(this_source_row)

        return source_rows


class importerIB_2018():
    def __init__(self, filePath):
        self.filePath = filePath

    def readFile(self):
        rows_ib = []
        with open(self.filePath, 'r+') as f:
            file_content = f.read()
            file_content = file_content.replace('"', '')
            file_content_list_of_lines = file_content.split("\n")
            file_content_list_of_lines = [line for line in file_content_list_of_lines if len(
                line) > 0]  # Briše prazne vrstice
            file_content_list_of_lists_of_columns = []
            for line in file_content_list_of_lines:
                file_content_list_of_lists_of_columns.append(line.split(","))
            header_row = file_content_list_of_lists_of_columns[0]
            del(file_content_list_of_lists_of_columns[0])
            for row in file_content_list_of_lists_of_columns:
                this_row_dict = {}
                for i in range(len(row)):
                    this_row_dict[header_row[i]] = row[i]
                rows_ib.append(this_row_dict)

        # filtriranje pravih asset class

        rows_ib = [row for row in rows_ib if row["AssetClass"] != "CASH" and not (
            row["AssetClass"] == "CMDTY" and row["Symbol"].lower() in ('xauusd', 'xagusd'))]
        source_rows = []

        # Mapiranje do kod asset classov
        asset_classes_dict = {"STK": 0, "FUT": 1,
            "CMDTY": 2, "CFD": 2, "OPT": 3, "FOP": 1}

        # priprava končnega outputa, po vsaki vrstici
        for row in rows_ib:
            this_source_row = []
            this_source_row.append(row["Symbol"])

            if row["Buy/Sell"] == "SELL":
                this_source_row.append("S")
            elif row["Buy/Sell"] == "BUY":
                this_source_row.append("B")
            else:
                raise Exception(
                    "Trade Type is {:s}, which is not 'BUY' or 'SELL'".format(row["Buy/Sell"]))

            this_source_row.append(datetime.strptime('{:s}{:s}'.format(
                row["TradeDate"], row["TradeTime"]), '%Y%m%d%H%M%S'))
            this_source_row.append(abs(Decimal(row["Quantity"])))
            this_source_row.append(Decimal(row["TradePrice"]))
            this_source_row.append(asset_classes_dict[row["AssetClass"]])
            this_source_row.append(row["CurrencyPrimary"])
            this_source_row.append(Decimal(row["Multiplier"]))
            source_rows.append(this_source_row)

        return source_rows


class importerIB():
    def __init__(self, filePath):
        self.filePath = filePath

    def readFile(self):
        rows_ib = []
        with open(self.filePath, 'r+') as f:
            file_content = f.read()
            file_content = file_content.replace('"', '')
            file_content_list_of_lines = file_content.split("\n")
            file_content_list_of_lines = [line for line in file_content_list_of_lines if len(
                line) > 0]  # Briše prazne vrstice
            file_content_list_of_lists_of_columns = []
            for line in file_content_list_of_lines:
                file_content_list_of_lists_of_columns.append(line.split(","))
            header_row = file_content_list_of_lists_of_columns[0]
            del(file_content_list_of_lists_of_columns[0])
            for row in file_content_list_of_lists_of_columns:
                this_row_dict = {}
                for i in range(len(row)):
                    this_row_dict[header_row[i]] = row[i]
                rows_ib.append(this_row_dict)

        # filtriranje pravih asset class

        rows_ib = [row for row in rows_ib if row["AssetClass"] != "CASH" and not (
            row["AssetClass"] == "CMDTY" and row["Symbol"].lower() in ('xauusd', 'xagusd'))]
        source_rows = []

        # Mapiranje do kod asset classov
        asset_classes_dict = {"STK": 0, "FUT": 1,
            "CMDTY": 2, "CFD": 2, "OPT": 3, "FOP": 1}

        # priprava končnega outputa, po vsaki vrstici
        for row in rows_ib:
            this_source_row = []
            this_source_row.append(row["Symbol"])

            if row["Buy/Sell"] == "SELL":
                this_source_row.append("S")
            elif row["Buy/Sell"] == "BUY":
                this_source_row.append("B")
            else:
                raise Exception(
                    "Trade Type is {:s}, which is not 'BUY' or 'SELL'".format(row["Buy/Sell"]))

            this_source_row.append(datetime.strptime(
                '{:s}'.format(row["DateTime"]), '%Y%m%d;%H%M%S'))
            this_source_row.append(abs(Decimal(row["Quantity"])))
            this_source_row.append(Decimal(row["TradePrice"]))
            this_source_row.append(asset_classes_dict[row["AssetClass"]])
            this_source_row.append(row["CurrencyPrimary"])
            this_source_row.append(Decimal(row["Multiplier"]))
            source_rows.append(this_source_row)

        return source_rows


class importerIB_2022():
    def __init__(self, filePath):
        self.filePath = filePath

        self.asset_classes_dict = {
            'CFDs - Held with Interactive Brokers (U.K.) Limited': 2,
            'Stocks - Held with Interactive Brokers (U.K.) Limited carried by Interactive Brokers LLC': 0,
            'Stocks': 0,
            'Equity and Index Options': 3,
            'Forex CFDs - Held with Interactive Brokers (U.K.) Limited carried by Interactive Brokers LLC': 2,
            'Commodities - Held with Interactive Brokers (U.K.) Limited': 1
        }

    def readFile(self):
        """
        Go through each row
        validate each header
        """

        source_rows = []
        with open(self.filePath, 'r+') as f:
            reader = csv.DictReader(f)
            rows = [line for line in reader]

        for row in rows:
            # Only process rows if it contains data

            if not row['Header'] == 'Data':
                continue

            if not row['Trades'] == 'Trades':
                continue

            def convertTime(string):
                ts = datetime.strptime(string, '%Y-%m-%d, %H:%M:%S')
               
                return ts

            ticker = row['Symbol']
            buysell = 'S' if row['Quantity'][0] == '-' else 'B'
            timestamp_open = convertTime(row['Date/Time'])

            quantity_clean = row['Quantity'].replace(',','')


            quantity = abs(Decimal(quantity_clean))
            price = Decimal(row['T. Price'])
            trade_type = self.asset_classes_dict[row['Asset Category']]
            currency = row['Currency']
            multiplier = 1
        
            this_source_row = [ticker,
                                        buysell,
                                        timestamp_open,
                                        quantity,
                                        price,
                                        trade_type,
                                        currency,
                                        multiplier]

            source_rows.append(this_source_row)
        
        return source_rows

class importerSpeed2022():
    def __init__(self, filePath):
        # All instrument stocks, USD
        self.filePath = filePath

        self.trades_dict = {
        "SS": "S",
        "BC": "B",
        "B": "B",
        "S": "S",
        "Sell": "S",
        "Buy": "B"
        }


    def readFile(self):
        """

        """
        def convertTime(string):
        
            ts = datetime.strptime(string, '%m/%d/%Y %H:%M')

            return ts

        source_rows = []
        with open(self.filePath, 'r+') as f:
            reader = csv.DictReader(f)
            rows = [line for line in reader]

        for row in rows:

            ticker = row['Symbol']
            buysell = self.trades_dict[row['Buy']]
            timestamp_open = convertTime(row['Execution Time'])

            quantity = Decimal(row['Size'])

            price = Decimal(row['Price'])
            trade_type = 0
            currency = 'USD'
            multiplier = 1
        
            this_source_row = [ticker,
                                        buysell,
                                        timestamp_open,
                                        quantity,
                                        price,
                                        trade_type,
                                        currency,
                                        multiplier]

            source_rows.append(this_source_row)
        
        return source_rows            
class importerCsv():
    def __init__(self, filePath):
        self.filePath = filePath

    def readFile(self):
        """

        """
        def convertTime(string):
        
            ts = datetime.strptime(string, '%Y-%m-%d %H:%M:%S')

            return ts

        source_rows = []
        with open(self.filePath, 'r+') as f:
            reader = csv.DictReader(f)
            rows = [line for line in reader]

        for row in rows:

            if row["Size"] == "0":
                continue

            ticker = row['Ticker']
            buysell = row['Buysell']
            timestamp_open = convertTime(row['TradeTime'])

            quantity = Decimal(row['Size'])

            price = Decimal(row['Price'])
            trade_type = int(row["Type"])
            currency = row['Currency']
            multiplier = Decimal(row['Multiplier'])
        
            this_source_row = [ticker,
                                buysell,
                                timestamp_open,
                                quantity,
                                price,
                                trade_type,
                                currency,
                                multiplier]

            source_rows.append(this_source_row)
        
        return source_rows            

class importerCobraSterling2021():
    def __init__(self, file_path):
        self.file_path = file_path

    def readFile(self):
        book = open_workbook(self.file_path)
        sheet = book.sheet_by_index(0)

        def convertDateOrReturn(value):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same

            datetuple = xldate_as_tuple(value, 0)
            return datetime(*datetuple)
            """
            if isinstance(value, str):
                date_obj = datetime.strptime(value, '%Y.%m.%d %H:%M:%S')
            else:
                datetuple = xldate_as_tuple(value, 0)
                date_obj = datetime(*datetuple)

            return date_obj

        self.data = []

        row_with_headers_for_trades = 0

        for x in range(1, sheet.nrows):
            if sheet.cell(x,1) == 'Canceled':
                continue

            thisRow = OrderedDict()
            for y in range(sheet.ncols):
                thisRow[sheet.cell(row_with_headers_for_trades, y).value] = sheet.cell(x, y).value
            self.data.append(thisRow)

        headers = {
            'ticker': 'Symbol',
            'buysell': 'Side',
            'timestamp': 'Time',
            'quantity': 'Order Quantity',
            'price': 'Exe Price'
        }

        source_rows = []

        for row in self.data:

            this_source_row = []
            this_source_row.append(row[headers['ticker']])

            if row[headers['buysell']] in ('S', 'T'):
                this_source_row.append('S')
            elif row[headers['buysell']] == 'B':
                this_source_row.append('B')
            else:
                raise Exception('This buysell value ({:s}) is not valid'.format(row[headers['buysell']]))

            this_source_row.append(
                    convertDateOrReturn(row[headers['timestamp']]))
            this_source_row.append(abs(Decimal(row[headers['quantity']])))
            this_source_row.append(Decimal(row[headers['price']]))
            this_source_row.append(0) #Vedno stock
            this_source_row.append('USD')
            this_source_row.append(1) #multiplier

            source_rows.append(this_source_row)

        return source_rows

class importerCobraGeneric2021():
    def __init__(self, file_path):
        self.file_path = file_path

    def readFile(self):
        book = open_workbook(self.file_path)
        sheet = book.sheet_by_index(0)

        def convertDateOrReturncombined(value1, value2):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same

            """
            datetuple1 = xldate_as_tuple(value1, 0)

            if type(value2) == float:
                datetuple2 = xldate_as_tuple(value2, 0)
                if datetuple2[0] == 0:
                    datetuple = (datetuple1[0], 
                                datetuple1[1],
                                datetuple1[2],
                                datetuple2[3],
                                datetuple2[4],
                                datetuple2[5])
                else:
                    datetuple = datetuple2
            elif type(value2) == str:
                datetuple2 = time(int(value2[:2]), int(value2[3:5]), int(value2[6:8]))
                datetuple = (datetuple1[0], 
                                datetuple1[1],
                                datetuple1[2],
                                datetuple2.hour,
                                datetuple2.minute,
                                datetuple2.second)

                print(value1, value2, datetuple)
            else:
                raise Exception
            
            date_obj = datetime(*datetuple)

            return date_obj

        self.data = []

        row_with_headers_for_trades = 0

        for x in range(1, sheet.nrows):

            thisRow = OrderedDict()
            for y in range(sheet.ncols):
                thisRow[sheet.cell(row_with_headers_for_trades, y).value] = sheet.cell(x, y).value
            self.data.append(thisRow)

        headers = {
            'ticker': 'Symbol',
            'buysell': 'Type',
            'timestamp': 'Date', #time manually added
            'quantity': 'Volume',
            'price': 'Price',
            'currency': 'Currency'
        }

        source_rows = []

        for row in self.data:

            this_source_row = []
            this_source_row.append(row[headers['ticker']])


            if row[headers['buysell']] == 'BUY':
                this_source_row.append('B')
            elif row[headers['buysell']] == 'SELL':
                this_source_row.append('S')
            else:
                raise Exception

            

            this_source_row.append(
                    convertDateOrReturncombined(row[headers['timestamp']], row['Time']))
            this_source_row.append(abs(Decimal(row[headers['quantity']])))
            this_source_row.append(Decimal(row[headers['price']]))
            this_source_row.append(0) #Vedno cfd
            this_source_row.append(row[headers['currency']])
            this_source_row.append(1) #multiplier

            source_rows.append(this_source_row)

        return source_rows

class importerCobraDas2021():
    def __init__(self, file_path):
        self.file_path = file_path

    def readFile(self):
        book = open_workbook(self.file_path)
        sheet = book.sheet_by_index(0)

        def convertDateOrReturn(value):
            """
            Function takes excel cell value formatted,
            returns date object if possible, else same

            datetuple = xldate_as_tuple(value, 0)
            return datetime(*datetuple)
            """
            if isinstance(value, str):
                date_obj = datetime.strptime(value, '%Y.%m.%d %H:%M:%S')
            else:
                datetuple = xldate_as_tuple(value, 0)
                date_obj = datetime(*datetuple)

            return date_obj

        self.data = []

        row_with_headers_for_trades = 0

        for x in range(1, sheet.nrows):

            thisRow = OrderedDict()
            for y in range(sheet.ncols):
                thisRow[sheet.cell(row_with_headers_for_trades, y).value] = sheet.cell(x, y).value
            self.data.append(thisRow)

        headers = {
            'ticker': 'Symbol',
            'buysell': 'BuySell',
            'timestamp': 'EvtDate',
            'quantity': 'Shares',
            'price': 'Price'
        }

        source_rows = []

        for row in self.data:

            this_source_row = []
            this_source_row.append(row[headers['ticker']])


            if row[headers['buysell']] in ('S', 'SS', 'SL'):
                this_source_row.append('S')
            elif row[headers['buysell']] in ('B', 'BL', 'BC'):
                this_source_row.append('B')

            this_source_row.append(
                    convertDateOrReturn(row[headers['timestamp']]))
            this_source_row.append(abs(Decimal(row[headers['quantity']])))
            this_source_row.append(Decimal(row[headers['price']]))
            this_source_row.append(0) #Vedno cfd
            this_source_row.append('USD')
            this_source_row.append(1) #multiplier

            source_rows.append(this_source_row)

        return source_rows

class importerResiduals():
    def __init__(self,filePath):
        self.filePath = filePath

    def readFile(self):
        rows_res = []
        with open(self.filePath,'r+', encoding='utf-8') as f:
            file_content = f.read()
            file_content_list_of_lines = file_content.split("\n")
            file_content_list_of_lines = [line for line in file_content_list_of_lines if len(line) > 0] # Briše prazne vrstice
            file_content_list_of_lists_of_columns = []
            for line in file_content_list_of_lines:
                file_content_list_of_lists_of_columns.append(line.split(","))
            header_row = file_content_list_of_lists_of_columns[0]
            del(file_content_list_of_lists_of_columns[0])
            for row in file_content_list_of_lists_of_columns:
                this_row_dict = {}
                for i in range(len(row)):
                    this_row_dict[header_row[i]] = row[i]
                rows_res.append(this_row_dict)
        
        rows_res = [row for row in rows_res]
        
        source_rows = []

        for row in rows_res:
            this_source_row = []
            this_source_row.append(row["instrument"])       
            this_source_row.append(row["buysell"])

            row['čas'] = row['čas'].replace('-',':')

            stringForDatetime = '{:s} {:s}'.format(row['datum'],row['čas'])
            this_source_row.append(datetime.strptime(stringForDatetime,'%Y-%m-%d %H:%M:%S'))
            this_source_row.append(Decimal(row["količina"]))
            this_source_row.append(Decimal(row["cena"]))
            # row['tip instrumenta'] = '0{:d}'.format(row['tip instrumenta']) if len(row['tip instrumenta']) == 1 else row['tip instrumenta']
            this_source_row.append(int(row["tip instrumenta"]))
            this_source_row.append(row["valuta instrumenta"])
            this_source_row.append(Decimal(row["multiplikator"]))
            source_rows.append(this_source_row)

        return source_rows

class importerTradeStation():
    def __init__(self,filePath):
        self.filePath = filePath

    def readFile(self):
        rows_tradestation = []
        with open(self.filePath,'r+') as f:
            file_content = f.read()
            file_content = file_content.replace('"','')
            file_content_list_of_lines = file_content.split("\n")
            file_content_list_of_lines = [line for line in file_content_list_of_lines if len(line) > 0] # Briše prazne vrstice
            file_content_list_of_lists_of_columns = []
            for line in file_content_list_of_lines:
                file_content_list_of_lists_of_columns.append(line.split(","))
            header_row = file_content_list_of_lists_of_columns[0]
            del(file_content_list_of_lists_of_columns[0])
            for row in file_content_list_of_lists_of_columns:
                this_row_dict = {}
                for i in range(len(row)):
                    this_row_dict[header_row[i]] = row[i]
                rows_tradestation.append(this_row_dict)
                
        source_rows = []
        
        def find_nth(haystack, needle, n): # funkcija za iskanje datuma
            start = haystack.find(needle)
            while start >= 0 and n > 1:
                start = haystack.find(needle, start+len(needle))
                n -= 1
            return start

        for row in rows_tradestation:
            this_source_row = []

            symbol = row["Symbol"]
            clean_symbol = symbol.replace(" ","")
            this_source_row.append(clean_symbol)
            
            if row["Transaction"] == "Sell":
                this_source_row.append("S")
            elif row["Transaction"] == "Buy":
                this_source_row.append("B")
            else:
                raise Exception("Trade Type is not 'BUY' or 'Sell'")
            timestamp_ts = row["Activity Time"]
            month_end = find_nth(timestamp_ts, "/", 1)
            day_start = month_end + 1
            day_end = find_nth(timestamp_ts, "/", 2)
            year_start = day_end + 1
            year_end = year_start + 4
            time_start = 1 + find_nth(timestamp_ts, " ",1)
            
            year = timestamp_ts[year_start:year_end]
            month = timestamp_ts[:month_end]
            if len(month) == 1:
                month = "0" + month
            day = timestamp_ts[day_start:day_end]
            if len(day) == 1:
                day = "0" + day             

            this_source_row.append(year + "-" + month + "-" + day)
            this_source_row.append(timestamp_ts[time_start:time_start + 2] + "-" + timestamp_ts[time_start + 3:time_start + 5] + "-" + timestamp_ts[time_start + 6:time_start + 8])

            this_source_row.append(abs(float(row["Quantity"])))
            this_source_row.append(float(row["Price"]))
            this_source_row.append(0) # Vse iz Tradestation so delnice
            this_source_row.append(row["CurrencyCode"])
            this_source_row.append(1.0) # Mulitplikator je vedno float
            source_rows.append(this_source_row)
        
        return source_rows

class importerEtoro():
    """
    Reads the closed positions tab (assumes to be second in a row) and generates open and closed trades
    The columns in the file are:

    Position ID
    Action
    Copy Trader Name
    Amount
    Units
    Open Rate
    Close Rate
    Spread
    Profit
    Open Date
    Close Date
    Take Profit Rate
    Stop Loss Rate
    Rollover Fees And Dividends
    Is Real

    """
    def __init__(self, filepath, existing_registry):

        book = open_workbook(filepath)
    
        self.sheet = book.sheet_by_index(0) if len(book.sheets()) == 1 else book.sheet_by_index(1)

        self.existing_registry = existing_registry

        self.asset_classes_dict = {
            'CFD': 2,
            'Real': 0,
            'Stocks': 0,
            'ETF': 0
        }

        self.dict_valut = load_xml()

        self.instruments_suggested_currencies = {}

    def write_instrument_suggested_currencies(self):
        with open('etoro_tickers_to_instruments_suggested.txt', 'a') as f:    
            writer = csv.writer(f, delimiter = '|')
            for instrument in self.instruments_suggested_currencies:
                writer.writerow([instrument, self.instruments_suggested_currencies[instrument]])

    def convertDateOrReturn(value):
        """
        Function takes excel cell value formatted,
        returns date object if possible, else same
        """
        try:
            datetuple = xldate_as_tuple(value, 0)
            print(datetime(*datetuple))
            raise Exception
            return datetime(*datetuple)

        except:
            return value


    def convertAndSplitDateFromString(self, value):

        ts = None

        formats = ['%d.%m.%Y %H:%M:%S', 
                    '%d.%m.%Y %H:%M', 
                    '%d/%m/%Y %H:%M:%S',
                    '%d/%m/%Y %H:%M']

        for format in formats:

            try:
                ts = datetime.strptime(value, format)
            except:
                continue
        if ts:
            return ts 
        else:
            raise Exception('Konverzija časa ni uspela. {}'.format(value))

    def find_header_index(self, list_of_headers):
        header_indices = {
            'Action': None,
            'Open Date': None,
            'Close Date': None,
            'Units': None,
            'Open Rate': None,
            'Close Rate': None,
            'Type': None
        }

        for header in header_indices:
            try:
                header_indices[header] = list_of_headers.index(header) + 1
            except:
                raise Exception('Stolpca {} ni v prvi vrstici datoteke.'.format(header))

        return header_indices

    def readFile(self):

        def sanitize_numeric(string):
            if isinstance(string, str):
                return string.replace(',', '.')
            else:
                return string

        first_row = [self.sheet.cell(0,x).value for x in range(1, self.sheet.ncols)]

        header_indices = self.find_header_index(first_row)
        
        source_rows = []
        
        for x in range(1, self.sheet.nrows):


            if self.sheet.cell(x, header_indices['Type']).value == 'Crypto': #skipping these rows
                continue

            if self.sheet.cell(x, 0).value == '': #skipping empty rows
                continue

            def split_ticker_buysell(ticker_buysell):

                if 'buy' in ticker_buysell.lower():
                    return (ticker_buysell[4:].replace(',', ' '), 'B')
                elif 'sell' in ticker_buysell.lower():
                    return (ticker_buysell[5:].replace(',', ' '), 'S')

            """
            V row dodajaš:
            ticker
            buysell
            timestamp
            quantity
            price
            trade_type
            currency
            multiplier
            """


            # get ticker
            ticker_buysell = self.sheet.cell(x, header_indices['Action']).value

            ticker, buysell = split_ticker_buysell(ticker_buysell)

            buysell_close = 'B' if buysell == 'S' else 'S'

            ts_open_str = self.sheet.cell(x,header_indices['Open Date']).value

            timestamp_open = self.convertAndSplitDateFromString(ts_open_str)

            ts_close_str = self.sheet.cell(x,header_indices['Close Date']).value
            timestamp_close = self.convertAndSplitDateFromString(ts_close_str)

            quantity = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Units']).value))

            price_open = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Open Rate']).value))
            
            price_close = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Close Rate']).value))

            trade_type = self.asset_classes_dict[self.sheet.cell(x,header_indices['Type']).value]

            currency = 'UNKNOWN'

            multiplier = Decimal(1)

            # Etoro does not specify the currency, this is a check if the currency needs to be replaced

            if ticker in self.existing_registry: # Do we already have it?
                logger.info("Instrument {} found in registry".format(ticker))
                currency = self.existing_registry[ticker]['currency']
                multiplier = Decimal(self.existing_registry[ticker]['multiplier'])

            else:

                if 'Leverage' in first_row: # We can check for alternative currencies if the source file contains the leverage column

                    openxunits = Decimal(sanitize_numeric(self.sheet.cell(x, header_indices['Open Rate']).value)) * \
                    Decimal(sanitize_numeric(self.sheet.cell(x, 3).value))
                    
                    leveragexamount = Decimal(sanitize_numeric(self.sheet.cell(x, header_indices['']).value)) * \
                    Decimal(sanitize_numeric(self.sheet.cell(x, 6).value))

                    actual_ratio = leveragexamount/openxunits

                    # check the ratio and compare it to the ratio to the USD for that day.

                    def are_ratios_close(ratio1, ratio2):
                        ratio = ratio1/ratio2
                        if ratio > 0.98 and ratio < 1.02:
                            return True
                        else:
                            return False

                    if are_ratios_close(leveragexamount,openxunits) == True:
                        currency = 'USD'
                        self.instruments_suggested_currencies[ticker] = '{}'.format(currency)
                        logger.info("Instrument {} seems to be USD because leverage x amount is same as open x units.".format(ticker))
                    
                    else:

                        suggested_currency = 'UNKNOWN'

                        # try to find the alternative
                        currencies_on_the_day = get_rates_for_all_currencies(timestamp_close, self.dict_valut)

                        # We have the actual ratio. Which currency has a close ratio? Convert via EUR!

                        potential_ratios = {}

                        potential_ratios['EUR'] = currencies_on_the_day['USD']
                        potential_ratios['GBP'] = currencies_on_the_day['USD']/currencies_on_the_day['GBP']
                        potential_ratios['JPY'] = currencies_on_the_day['USD']/currencies_on_the_day['JPY']
                        potential_ratios['GBX'] = currencies_on_the_day['USD']/currencies_on_the_day['GBP'] / 100
                        potential_ratios['DKK'] = currencies_on_the_day['USD']/currencies_on_the_day['DKK']
                        potential_ratios['SEK'] = currencies_on_the_day['USD']/currencies_on_the_day['SEK']
                        potential_ratios['HKD'] = currencies_on_the_day['USD']/currencies_on_the_day['HKD']

                        for currency in potential_ratios:

                            if are_ratios_close(actual_ratio, potential_ratios[currency]) == True:
                                if currency == "GBX":
                                    currency = "GBP"
                                suggested_currency = currency
                                logger.info("Instrument {} found seems to be {}, because leverage x amount is same as open x units when converted to {}".format(ticker, currency, currency))
                                
                                break

                        self.instruments_suggested_currencies[ticker] = '{}'.format(suggested_currency)

                else:
                    self.instruments_suggested_currencies[ticker] = '{}'.format(currency)

             
                        
            this_source_row_open = [ticker,
                                        buysell,
                                        timestamp_open,
                                        quantity,
                                        price_open,
                                        trade_type,
                                        currency,
                                        multiplier]

            this_source_row_close = [ticker,
                                        buysell_close,
                                        timestamp_close,
                                        quantity,
                                        price_close,
                                        trade_type,
                                        currency,
                                        multiplier]
            
            source_rows.append(this_source_row_open)
            source_rows.append(this_source_row_close)
        
        self.write_instrument_suggested_currencies()

        return source_rows

class importerEtoro2025():
    """
    Reads the closed positions tab (assumes to be second in a row) and generates open and closed trades
    The columns in the file are:

    Position ID
    Action
    Long / Short
    Amount
    Units / Contracts
    Open Date
    Close Date
    Leverage
    Spread Fees (USD)
    Market Spread (USD)
    Profit(USD)
    Profit(EUR)
    FX rate at open (USD)
    FX rate at close (USD)
    Open Rate
    Close Rate
    Take profit rate
    Stop loss rate
    Overnight Fees and Dividends
    Copied From
    Type
    ISIN
    Notes

    These used to be the columns, fix the logic to match the columnts above

    Position ID
    Action
    Copy Trader Name
    Amount
    Units
    Open Rate
    Close Rate
    Spread
    Profit
    Open Date
    Close Date
    Take Profit Rate
    Stop Loss Rate
    Rollover Fees And Dividends
    Is Real

    """
    def __init__(self, filepath, existing_registry):

        book = open_workbook(filepath)
    
        self.sheet = book.sheet_by_index(0) if len(book.sheets()) == 1 else book.sheet_by_index(1)

        self.existing_registry = existing_registry

        self.asset_classes_dict = {
            'CFD': 2,
            'Real': 0,
            'Stocks': 0,
            'ETF': 0
        }

        self.dict_valut = load_xml()

        self.instruments_suggested_currencies = {}

    def write_instrument_suggested_currencies(self):
        with open('etoro_tickers_to_instruments_suggested.txt', 'a') as f:    
            writer = csv.writer(f, delimiter = '|')
            for instrument in self.instruments_suggested_currencies:
                writer.writerow([instrument, self.instruments_suggested_currencies[instrument]])

    def convertDateOrReturn(value):
        """
        Function takes excel cell value formatted,
        returns date object if possible, else same
        """
        try:
            datetuple = xldate_as_tuple(value, 0)
            print(datetime(*datetuple))
            raise Exception
            return datetime(*datetuple)

        except:
            return value


    def convertAndSplitDateFromString(self, value):

        ts = None

        formats = ['%d.%m.%Y %H:%M:%S', 
                    '%d.%m.%Y %H:%M', 
                    '%d/%m/%Y %H:%M:%S',
                    '%d/%m/%Y %H:%M']

        for format in formats:

            try:
                ts = datetime.strptime(value, format)
            except:
                continue
        if ts:
            return ts 
        else:
            raise Exception('Konverzija časa ni uspela. {}'.format(value))

    def find_header_index(self, list_of_headers):
        header_indices = {
            'Action': None,
            'Open Date': None,
            'Close Date': None,
            'Units / Contracts': None,
            'Open Rate': None,
            'Close Rate': None,
            'Type': None,
            'Long / Short': None
        }

        for header in header_indices:
            try:
                header_indices[header] = list_of_headers.index(header) + 1
            except:
                raise Exception('Stolpca {} ni v prvi vrstici datoteke.'.format(header))

        return header_indices

    def readFile(self):

        def sanitize_numeric(string):
            if isinstance(string, str):
                return string.replace(',', '.')
            else:
                return string

        first_row = [self.sheet.cell(0,x).value for x in range(1, self.sheet.ncols)]

        header_indices = self.find_header_index(first_row)
        
        source_rows = []
        
        for x in range(1, self.sheet.nrows):

            if self.sheet.cell(x, header_indices['Type']).value == 'Crypto': #skipping these rows
                continue

            if self.sheet.cell(x, 0).value == '': #skipping empty rows
                continue

            long_short_dict = {
                'Long': 'B',
                'Short': 'S'
            }

            """
            V row se dodaja:
            ticker
            buysell
            timestamp
            quantity
            price
            trade_type
            currency
            multiplier
            """

            ticker = self.sheet.cell(x, header_indices['Action']).value

            buysell = long_short_dict[self.sheet.cell(x, header_indices['Long / Short']).value]

            buysell_close = 'B' if buysell == 'S' else 'S'

            ts_open_str = self.sheet.cell(x,header_indices['Open Date']).value

            timestamp_open = self.convertAndSplitDateFromString(ts_open_str)

            ts_close_str = self.sheet.cell(x,header_indices['Close Date']).value
            timestamp_close = self.convertAndSplitDateFromString(ts_close_str)

            quantity = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Units / Contracts']).value))

            price_open = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Open Rate']).value))
            
            price_close = Decimal(sanitize_numeric(self.sheet.cell(x,header_indices['Close Rate']).value))

            trade_type = self.asset_classes_dict[self.sheet.cell(x,header_indices['Type']).value]

            currency = 'UNKNOWN'

            multiplier = Decimal(1)

            # Etoro does not specify the currency, this is a check if the currency needs to be replaced

            if ticker in self.existing_registry: # Do we already have it?
                logger.info("Instrument {} found in registry".format(ticker))
                currency = self.existing_registry[ticker]['currency']
                multiplier = Decimal(self.existing_registry[ticker]['multiplier'])

            else:
                logger.info('Checking for alternative way to determine currency')

                if 'Leverage' in first_row: # We can check for alternative currencies if the source file contains the leverage column

                    leverage_index = first_row.index('Leverage') + 1
                    amount_index = first_row.index('Amount') + 1

                    openxunits = Decimal(sanitize_numeric(self.sheet.cell(x, header_indices['Open Rate']).value)) * \
                    Decimal(sanitize_numeric(self.sheet.cell(x, header_indices['Units / Contracts']).value))
                    
                    leveragexamount = Decimal(sanitize_numeric(self.sheet.cell(x, leverage_index).value)) * \
                    Decimal(sanitize_numeric(self.sheet.cell(x, amount_index).value))

                    actual_ratio = leveragexamount/openxunits

                    # check the ratio and compare it to the ratio to the USD for that day.

                    def are_ratios_close(ratio1, ratio2):
                        ratio = ratio1/ratio2
                        if ratio > 0.98 and ratio < 1.02:
                            return True
                        else:
                            return False

                    if are_ratios_close(leveragexamount,openxunits) == True:
                        currency = 'USD'
                        self.instruments_suggested_currencies[ticker] = '{}'.format(currency)
                        logger.info("Instrument {} seems to be USD because leverage x amount is same as open x units.".format(ticker))
                    
                    else:

                        suggested_currency = 'UNKNOWN'

                        # try to find the alternative
                        currencies_on_the_day = get_rates_for_all_currencies(timestamp_close, self.dict_valut)

                        # We have the actual ratio. Which currency has a close ratio? Convert via EUR!

                        potential_ratios = {}

                        potential_ratios['EUR'] = currencies_on_the_day['USD']
                        potential_ratios['GBP'] = currencies_on_the_day['USD']/currencies_on_the_day['GBP']
                        potential_ratios['JPY'] = currencies_on_the_day['USD']/currencies_on_the_day['JPY']
                        potential_ratios['GBX'] = currencies_on_the_day['USD']/currencies_on_the_day['GBP'] / 100
                        potential_ratios['DKK'] = currencies_on_the_day['USD']/currencies_on_the_day['DKK']
                        potential_ratios['SEK'] = currencies_on_the_day['USD']/currencies_on_the_day['SEK']
                        potential_ratios['HKD'] = currencies_on_the_day['USD']/currencies_on_the_day['HKD']

                        for currency in potential_ratios:

                            if are_ratios_close(actual_ratio, potential_ratios[currency]) == True:
                                if currency == "GBX":
                                    currency = "GBP"
                                suggested_currency = currency
                                logger.info("Instrument {} found seems to be {}, because leverage x amount is same as open x units when converted to {}".format(ticker, currency, currency))
                                
                                break

                        self.instruments_suggested_currencies[ticker] = '{}'.format(suggested_currency)

                else:
                    self.instruments_suggested_currencies[ticker] = '{}'.format(currency)

             
                        
            this_source_row_open = [ticker,
                                        buysell,
                                        timestamp_open,
                                        quantity,
                                        price_open,
                                        trade_type,
                                        currency,
                                        multiplier]

            this_source_row_close = [ticker,
                                        buysell_close,
                                        timestamp_close,
                                        quantity,
                                        price_close,
                                        trade_type,
                                        currency,
                                        multiplier]
                        
            source_rows.append(this_source_row_open)
            source_rows.append(this_source_row_close)
        
        self.write_instrument_suggested_currencies()

        return source_rows

def readSourceFile(fileName):
    """
    Function takes filename, returns correct function
    """
    # Excel import: Samo ti stolpci so lahko, v tem vrstnem redu. Datum kot excel datetime.
    # instrument    buysell datum   količina    cena    tip_instrumenta valuta_instrumenta  multiplikator

    filename_without_extension, ext = os.path.splitext(fileName)
    
        
    fileKeys = {
        "saxo": genImporterSaxo,
        "excel": genImporterExcel,
        "ib_initial": importerIB,
        "ib2022": importerIB_2022,
        "tradestation": importerTradeStation,
        "residuals": importerResiduals,
        "etoro_original": importerEtoro,
        "etoro2025": importerEtoro2025,
        "mt": importMetaTrader2021,
        "speed": importerSpeed2022,
        "cobra_sterling": importerCobraSterling2021,
        "cobra_das": importerCobraDas2021,
        "cobra_generic": importerCobraGeneric2021,
        "csv": importerCsv
    }

    foundKey = None

    for key in fileKeys:
        if key in filename_without_extension.lower():
            if foundKey is not None:
                raise Exception('Datoteka ima v imenu več kot en tip uvoza (tipi so: {:s})'.format(','.join(fileKeys.keys())))
            
            foundKey = key
    if foundKey is None:
        raise Exception('Ni izvorne datoteke, ki bi vsebovala kaj od tega: {:s}'.format(','.join(fileKeys.keys()))) 
    correctClass = fileKeys[foundKey]

    return correctClass

