import datetime
import os
import pickle
import re
import time
from pprint import pprint
from xml.dom.minidom import parse
import logging
from decimal import *
from xlrd import XL_CELL_DATE, open_workbook, xldate_as_tuple

def dataToTimelines(data):
	"""
	vsak instrument v svojo časovno vrsto (naredi se dict)
	"""
	timelines = {}
	for row in data:
		unique_key = (row[0], row[5], row[6],row[7])
		if unique_key in timelines:
			timelines[unique_key].append(row)
		else:
			timelines.update({unique_key:[]})
			timelines[unique_key].append(row)
	return timelines


def cleanFilenames(files):
	if '.DS_Store' in files:
		files.remove('.DS_Store')
	for file in files:
		if file[:1] == '~':
			files.remove(file)
	return files

def getFilesFromSource(sourceFolder):
	"""
	Function return files from Application Source Folder
	"""
	files = cleanFilenames(os.listdir(sourceFolder))

	if len(files) == 0:
		raise Exception('No files in source folder {:s}'.format(sourceFolder))

	return files

def getPersonDataFromFiles(listOfFiles):
	"""
	Tale funkcija gre čez imena fajlov, dobi ven Ime in priimek, davčno. Preveri tudi, če je več kot ena davčna ali več kot eno ime. Obstaja tudi možnost dodajanja lastnega imena.
	"""
	personData = {
		"tax_number":None,
		"name":None
		}

	taxPattern = re.compile(r"^[\d]{8}") # Regex match v imenu datoteke, če je vpisana davčna
	namePattern = re.compile(r"(?<=#).*(?=#)") # Regex match v imenu datoteke, če je vpisano ime

	for filename in listOfFiles:
		if taxPattern.search(filename):
			tax_number = taxPattern.findall(filename)[0]
			if personData["tax_number"] is None:
				personData["tax_number"] = tax_number
			elif personData["tax_number"] != tax_number:
				raise Exception("V mapi Source so davčne več strank.")
		if namePattern.search(filename):
			name = namePattern.findall(filename)[0]
			if personData["name"] is None:
				personData["name"] = name

	return personData

def writeFile(output_folder,string,tax_number,person_name,output_type, is_residual=False, year=None):
	"""
	"""
	outputTypes = {
		'residuals':('Residuals_','csv'),
		'ifi':('IFI_','xml'),
		'kdvp':('KDVP_','xml'),
		'div':('DIV_','xml'),
	}
	if is_residual:
		output_folder = os.path.join(output_folder, year)
		filename = '{:s} #{:s}# {:s}.{:s}'.format(tax_number,person_name,outputTypes[output_type][0],outputTypes[output_type][1])	
	else:
		filename = '{:s} #{:s}# {:s}_{:%Y-%m-%d %H-%M-%S}.{:s}'.format(tax_number,person_name,outputTypes[output_type][0],datetime.datetime.now(),outputTypes[output_type][1])

	if not os.path.exists(os.path.join(output_folder,tax_number)):
		print('{} does not exist'.format(os.path.join(output_folder,tax_number)))
		os.makedirs(os.path.join(output_folder,tax_number))
		print('{} created'.format(os.path.join(output_folder,tax_number)))
		
	with open(os.path.join(output_folder,tax_number,filename),'w', encoding='utf-8') as f:
		f.write(string)

def archiveFile(output_folder,tax_number):
	"""
	Check if destination folder exists
	move all from output folder to destination foler
	"""
	destination_folder = os.path.join(output_folder, 'archived', tax_number)
	source_folder = os.path.join(output_folder, tax_number)

	if not os.path.exists(destination_folder):
		os.makedirs(destination_folder)

	if os.path.exists(source_folder):
		for file in os.listdir(source_folder):
			os.rename(os.path.join(source_folder, file), os.path.join(destination_folder, file))

def askForPersonData():
	"""
	Ask for Name, Surname, Tax Number, renames all files in source folder when answered.
	"""
	person_name = input("Ime in priimek?\n")
	person_tax = input("Davčna številka?\n")
	person_data = (person_name, person_tax)

	for filename in cleanFilenames(os.listdir(APP_SOURCE_FOLDER)):
		os.rename(os.path.join(APP_SOURCE_FOLDER,filename), os.path.join(APP_SOURCE_FOLDER,'{:s} #{:s}# {:s}'.format(person_tax, person_name, filename)))

	personData = {
		"tax_number":person_name,
		"name":person_tax
		}

	return personData

# Branje xml-ja v dict
def load_xml():
	all_rates = {}
	try: # Poskusi 
		all_rates = pickle.load(open('xml_rates.p', 'rb'))
		print('Rates already loaded!')
	except:
		print('No rates available, reading xml from scratch.')		
		DOMTree = parse("exchange_rate/dtecbs-l.xml")
		collection = DOMTree.documentElement
		for dan in collection.getElementsByTagName("tecajnica"):
			key = datetime.datetime.strptime(dan.attributes["datum"].value,'%Y-%m-%d').date()
			all_rates[key] = {}

			for node in dan.getElementsByTagName("tecaj"):
				all_rates[key][node.attributes["oznaka"].value] = node.firstChild.data

		pickle.dump(all_rates, open('xml_rates.p', 'wb'))

	return all_rates

def load_instrument_currency_registry_etoro():

        with open('etoro_tickers_to_instruments.txt', 'r') as f:

            instruments_currency_registry = {line.split('|')[0]:{'currency':line.split('|')[1], 'multiplier':line.split('|')[2]} \
                                                for line in f.read().splitlines()}

        return instruments_currency_registry

# Pridobitev potrebnih tečajev
def get_rates_for_specific_currencies(tradetimes_and_currencies, xml_rates):
	""" pridobi vse datume in tečaje v dict, ki jih je treba najti 
	tradetimes_and_currencies - array of tuples -> tradetime, currency
	
	"""
	rates_to_find = {} # tukaj dajemo vse kombinacije, ki jih bomo potrebovali
	for row in tradetimes_and_currencies:
		tradeDate = row[0].date()
		currency = row[1]
		if tradeDate in rates_to_find: # Če imamo že datum v dictu, ne pa še valute, se jo doda.
			if currency not in rates_to_find[tradeDate]:
				rates_to_find[tradeDate].append(currency)
		else:
			rates_to_find.update({tradeDate:[]}) # Če še ni datuma, ga dodamo, nato pa dodamo valuto.
			rates_to_find[tradeDate].append(currency)

	def getRates(rates_to_find):
		rates = {} # se pripravi dict za return
		extracted = {} # podoben dict kot rates, ampak samo z datumi, ki so dejansko v tečajnici
		days_in_xml_rates = xml_rates.keys() # Seznam datumov, ki se pojavijo v tečajnici
		date_safe_versions = {} # dict za uparjanje dejanskih datumov poslov in morebitnih prilagojenih datumov.
		for trade_date in rates_to_find:
			trade_date_safe = trade_date # Kopija datuma se shrani pod trade_date_safe. Če datuma ni najti, se trade_date_safe zmanjšuje, dokler ne pride do datuma, ki ima tečaje. Pridobi se vse valute za ta dan, nato se shrani v rates dict z original_date, ki je za ostale primere enak date
			while not trade_date_safe in days_in_xml_rates:
				#date_object = datetime.datetime.strptime(trade_date_safe,"%Y-%m-%d")
				trade_date_safe = trade_date_safe - datetime.timedelta(days=1)
			date_safe_versions[trade_date] = trade_date_safe # dopolnjevanje dicta. Vsak trade date je key, ima value, ki se razlikuje samo, če trade_date ne obstaja v tečajnici. 
		
		for dan in xml_rates: # Sprehod v dict za podatke, ki jih potrebujemo

			if dan in date_safe_versions.values(): # če je datum enak trade_date_safe
				trade_dates = [trade_date for trade_date in date_safe_versions if date_safe_versions[trade_date] == dan] # Pridobimo keye iz dictionarya ki ustrezajo trenutno datumu, ki ga obdelujemo.
				for trade_date in trade_dates: # loop čez vse datume, ki imajo ta safe datum
					for currency in rates_to_find[trade_date]: # loop za vse potrebne valute
						if currency == "EUR": # EUR tečaja nikoli ni v XML-ju
							extracted[(dan,currency)] = 1 # polnjenje extracted
						else:
							this_rate = Decimal(xml_rates[dan][currency])
							extracted[(dan,currency)] = this_rate # polnjenje extracted						

		for trade_date in rates_to_find: # Sedaj loop čez vse dejanske datume posla, da prenesemo tečaje iz dicta extracted, kjer je key trade_date_safe namesto trade.
			for currency in rates_to_find[trade_date]:
				key_in_extracted = (date_safe_versions[trade_date],currency) # Za vsak posel ustvarimo key za iskanje v extracted, kjer uporabimo trade_date_safe
				rates[(trade_date,currency)] = extracted[key_in_extracted]
			
		return rates

	return getRates(rates_to_find)

def get_rates_for_all_currencies(tradetime, xml_rates):
	""" pridobi vse datume in tečaje v dict, ki jih je treba najti 
	tradetimes_and_currencies - array tradetimes, currency
	
	"""
	currencies_to_check = ('GBP', 'USD', 'JPY', 'DKK', 'SEK','HKD')
	trade_date = tradetime.date() # tukaj dajemo vse dni, ki jih bomo potrebovali


	rates = {} # se pripravi dict za return
	extracted = {} # podoben dict kot rates, ampak samo z datumi, ki so dejansko v tečajnici
	days_in_xml_rates = xml_rates.keys() # Seznam datumov, ki se pojavijo v tečajnici
	date_safe_versions = {} # dict za uparjanje dejanskih datumov poslov in morebitnih prilagojenih datumov.

	trade_date_safe = trade_date # Kopija datuma se shrani pod trade_date_safe. Če datuma ni najti, se trade_date_safe zmanjšuje, dokler ne pride do datuma, ki ima tečaje. Pridobi se vse valute za ta dan, nato se shrani v rates dict z original_date, ki je za ostale primere enak date
	while not trade_date_safe in days_in_xml_rates:
		#date_object = datetime.datetime.strptime(trade_date_safe,"%Y-%m-%d")
		trade_date_safe = trade_date_safe - datetime.timedelta(days=1)

	def extract_currency(day, currency):
		return Decimal(xml_rates[dan][currency])
	


	for dan in xml_rates: # Sprehod v dict za podatke, ki jih potrebujemo

		if dan == trade_date_safe: # če je datum enak trade_date_safe

			extracted = {currency: extract_currency(dan, currency) for currency in currencies_to_check}

	return extracted

def obtain_residuals(list_of_trades): # funkcija vzame seznam poslov. Vrne spremenjen seznam poslov in residuals
	list_of_residual_trades = []	
	stock = list_of_trades[-1][10] # Ali imamo na koncu pozitivno ali negativno
	list_of_trades = list_of_trades[::-1] # seznam poslov obrnemo
	if stock > 0:
		running_sum = 0
		for i in range(len(list_of_trades)):
			if list_of_trades[i][1] == "B":
				if list_of_trades[i][9] + running_sum < stock: # še ni doseglo praga
					list_of_residual_trades.append(list_of_trades[i])
					running_sum += list_of_trades[i][9]
				else:
					quantity_residual_trade = stock - running_sum
					new_residual_trade = [list_of_trades[i][0],list_of_trades[i][1],list_of_trades[i][2],list_of_trades[i][3],abs(quantity_residual_trade),list_of_trades[i][5],list_of_trades[i][6],list_of_trades[i][7],list_of_trades[i][8]]
					list_of_residual_trades.append(new_residual_trade)
					break
	elif stock < 0:
		running_sum = 0
		for i in range(len(list_of_trades)):
			if list_of_trades[i][1] == "S":
				if list_of_trades[i][9] - running_sum > stock: # še ni doseglo praga
					list_of_residual_trades.append(list_of_trades[i])
					running_sum += list_of_trades[i][9]
				else:
					quantity_residual_trade = stock - running_sum
					new_residual_trade = [list_of_trades[i][0],list_of_trades[i][1],list_of_trades[i][2],list_of_trades[i][3],abs(quantity_residual_trade),list_of_trades[i][5],list_of_trades[i][6],list_of_trades[i][7],list_of_trades[i][8]]
					list_of_residual_trades.append(new_residual_trade)
					break
	return list_of_residual_trades[::-1]

def obtain_residuals2(list_of_trades): # funkcija vzame seznam poslov. Vrne spremenjen seznam poslov in residuals
	list_of_trades_without_residuals = list_of_trades.copy()
	list_of_residual_trades = []
	stock = list_of_trades[-1][10] # Ali imamo na koncu pozitivno ali negativno
	list_of_trades = list_of_trades[::-1] # seznam poslov obrnemo
	if stock > 0:
		running_sum = 0
		for i in range(len(list_of_trades)):
			if list_of_trades[i][1] == "B":
				if list_of_trades[i][9] + running_sum < stock: # še ni doseglo praga
					list_of_residual_trades.append(list_of_trades[i])
					print('removing ' + str(list_of_trades_without_residuals[len(list_of_trades)-i-1]))
					del list_of_trades_without_residuals[len(list_of_trades)-i-1]
					running_sum += list_of_trades[i][9]
				else:
					quantity_residual_trade = stock - running_sum
					new_residual_trade = [list_of_trades[i][0],list_of_trades[i][1],list_of_trades[i][2],list_of_trades[i][3],abs(quantity_residual_trade),list_of_trades[i][5],list_of_trades[i][6],list_of_trades[i][7],list_of_trades[i][8]]
					list_of_residual_trades.append(new_residual_trade)
					break
	elif stock < 0:
		print('stock',stock)
		running_sum = 0
		for i in range(len(list_of_trades)):
			if list_of_trades[i][1] == "S":
				print(list_of_trades[i][9], running_sum)
				if list_of_trades[i][9] + running_sum > stock: # še ni doseglo praga
					list_of_residual_trades.append(list_of_trades[i])
					print('removing ' + str(list_of_trades_without_residuals[len(list_of_trades)-i-1]))
					del list_of_trades_without_residuals[len(list_of_trades)-i-1]
					running_sum += list_of_trades[i][9]
				else:
					quantity_residual_trade = stock - running_sum
					print(quantity_residual_trade)
					new_residual_trade = [list_of_trades[i][0],list_of_trades[i][1],list_of_trades[i][2],list_of_trades[i][3],abs(quantity_residual_trade),list_of_trades[i][5],list_of_trades[i][6],list_of_trades[i][7],list_of_trades[i][8]]
					list_of_residual_trades.append(new_residual_trade)
					break
	return (list_of_trades_without_residuals,list_of_residual_trades[::-1])

def does_list_contain_buy_and_sell(list_of_trades): # funkcija vzame seznam poslov. Vrne True, če so tako nakupi kot prodaje.
	sells = False
	buys = False
	for trade in list_of_trades:
		if trade[1] == "B":
			buys = True
		else:
			sells = True
	return (sells and buys)

def sanitize_name(text):
	sanitized = text
	forbidden_chars = ['&']
	for char in forbidden_chars:
		sanitized = sanitized.replace(char, '')

	return sanitized
