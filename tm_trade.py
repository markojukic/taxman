import datetime
from decimal import *
import logging

logger = logging.getLogger('taxman')

class Trade():
	def __init__(self,timestamp,quantity,buysell,price):
		self.timestamp = timestamp
		self.quantity = round(quantity, 4)
		self.buysell = buysell
		self.price = round(price, 4)

		self.quantity_for_output = None
		self.price_for_output = None
		self.stock_for_output = None

		if not isinstance(self.timestamp,datetime.datetime):
			raise TypeError('Timestamp {:s} is not of type datetime.'.format(str(self.timestamp)))

		if not isinstance(self.quantity, Decimal):
			raise TypeError('Quantity {:s} is not of type Decimal.'.format(str(self.quantity)))

		if not isinstance(self.buysell,str):
			raise TypeError('Buysell {:s} is not of type string.'.format(str(self.buysell)))

		if not isinstance(self.price, Decimal):
			raise TypeError('Price {:s} is not of type Decimal.'.format(str(self.price)))

		self.stock = None

	def splitTradeIfNeeded(self):
		""" Prilagodi količino. Vrne novo instanco s preostalo količino. Resetira zalogo. """
		if self.stock not in (None,0) and self.getSignedQuantity() / self.stock > 1: # Po poslu je zaloga bližje 0 kot je bil posel, zato smo vmes zamenjali pozicijo. Potreben je split.
			self.quantity = self.getSignedQuantity() - self.stock
			quantityForNewTrade = self.stock
			if self.buysell == 'S': # nazaj v absolutno vrednost za prodaje
				self.quantity = self.quantity * (-1)
				quantityForNewTrade = quantityForNewTrade * (-1)
			 
			splitTrade = Trade(self.timestamp, quantityForNewTrade, self.buysell, self.price)
			splitTrade.stock = self.stock
			self.stock = 0

			return splitTrade
		
		else:
			return False
	
	def splitTradeForced(self,quantityOfCreatedTrade):
		""" Razdeli posel na dva. Prvemu se zmanjša količina, drugi je vrnjen v funkciji. Argument je vedno pozitiven, torej za prodaje """
		if quantityOfCreatedTrade <= 0:
			raise Exception('New quantity argument for splitting trade cannot be 0 or negative.')
		self.quantity = self.quantity - quantityOfCreatedTrade
		splitTrade = Trade(self.timestamp,quantityOfCreatedTrade,self.buysell,self.price)
		
		return splitTrade

	def getSignedQuantity(self):
		""" Vrne količino s predznakom """
		return self.quantity * (-1) if self.buysell == 'S' else self.quantity
		
	def get_signed_value(self, multiplier):
		""" Vrne vrednost posla z vidika knjigovodstva """
		signature = -1 if self.buysell == 'B' else 1
		
		return self.quantity * signature * self.price * multiplier

	def __repr__(self):
		return "{},{},{},{},{}".format(self.timestamp, self.quantity, self.buysell, self.price, self.stock)
