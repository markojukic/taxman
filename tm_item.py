from tm_trade import Trade
import logging
import sys
import datetime
from dateutil.relativedelta import *

logger = logging.getLogger('taxman')

class Item():
	def __init__(self,name,type,currency,multiplier,trades):
		self.type = '0{:d}'.format(type)

		if type == 0:
			self.typename = "Delnica"
		elif type == 1:
			self.typename = "terminska pogodba"
		elif type == 2:
			self.typename = "finančne pogodbe na razliko"
		elif type == 3:
			self.typename = "opcija in certifikat"
		elif type == 4:
			self.typename = "Ostalo"
		else:
			raise Exception("Tip instrumenta ne obstaja ali ni IFI.")
		
		self.name = name.lstrip()
		self.ticker = self.name if len(self.name) < 11 else self.name[:10]
		self.fauxisin = self.ticker + ("x" * (12-len(self.ticker))) if len(self.ticker) < 12 else self.ticker[:12]
		self.currency = currency
		self.multiplier = multiplier

		self.tradeObjects = []

		for trade in trades:
			self.tradeObjects.append(Trade(trade[2],trade[3],trade[1],trade[4]))

		self.tradeObjectsDict = {'short':[], 'long':[]}
		self.residualTradeObjects = []
		
		self.profit_loss_pairs = {'short':[], 'long':[]}
		self.taxable = {
		        5: 0.0,
                10: 0.0,
                15: 0.0,
                20: 0.0
        	}
		
		if self.type != '00':
			self.taxable[1] = 0.0
		
		self.unclaimed_loss = 0.0
		
		self.group = 'stocks' if self.type == '00' else 'derivatives'
					
	def getPriceMultiplier(self):
		if self.type in ('00','02'):
			return self.multiplier
		else:
			return 1

	def getQuantityMultiplier(self):
		if self.type in ('00','02'):
			return 1
		else:
			return self.multiplier

	def addStockToTradesGeneric(self,trades):
		"""
		Vsakemu instrumentu se doda zalogo
		"""
		for idx,trade in enumerate(trades): #zaloga
			if idx == 0:
				trade.stock = trade.getSignedQuantity()
			else:
				stock_before = trades[idx-1].stock
				trade.stock = stock_before + trade.getSignedQuantity()

		return trades
	
	def addStockToTradeObjects(self):
		"""

		"""
		self.tradeObjects = self.addStockToTradesGeneric(self.tradeObjects)

	def addStockToTradeObjectsDict(self):
		"""

		"""
		self.tradeObjectsDict['long'] = self.addStockToTradesGeneric(self.tradeObjectsDict['long'])
		self.tradeObjectsDict['short'] = self.addStockToTradesGeneric(self.tradeObjectsDict['short'])

	def splitTradesThatSwitchPositions(self):
		"""
		Razbijanje poslov, ki gredo iz plus v minus ali obratno, na dva dela.
		"""
		for idx,trade in enumerate(self.tradeObjects):
			newTrade = trade.splitTradeIfNeeded()
			if newTrade:
				self.tradeObjects.insert(idx+1,newTrade)

	def populateLongShortLists(self):
		"""
		Razdelitev seznama poslov na long ali short pozicije
		"""
		for idx,trade in enumerate(self.tradeObjects):
			if idx == 0:
				if trade.buysell == 'B':
					self.tradeObjectsDict['long'].append(trade)
				else:
					self.tradeObjectsDict['short'].append(trade)

			else:
				if trade.stock > 0:
					self.tradeObjectsDict['long'].append(trade)

				elif trade.stock < 0:
					self.tradeObjectsDict['short'].append(trade)

				else:
					if trade.buysell == 'B':
						self.tradeObjectsDict['short'].append(trade)
					else:
						self.tradeObjectsDict['long'].append(trade)

	def moveResidualTrades(self):
		"""
		Odvečne posle premikamo v residuals, torej take, ki tvorijo nezaprte pozicije. 
		Tukaj lahko pride do nujnosti, da se prilagaja posle za nazaj, če je pozicija na koncu leta samo delno zaprta.
		1. Določimo opposite quantity in direction quantity
		2. Če ima pozicija samo istosmerne posle, gredo vsi v residuals 
		3. Če 2 ne drži, je treba ohraniti samo tiste istosmerne, ki tvorijo količino, kolikor smo prodali.
		Gremo čez posle, in iščemo istosmerni posel, ki prvi preseže količino zaprtja (kumulativno).
		Če je točno, potem je treba ostale istosmerne premakniti v residuals.
		Če preseže, se še prej naredi split, odvečni del gre v residuals.
		5. Na koncu se naredi preračun zalog za celotno vrsto.

		Obstaja še ena kategorija - popolnoma nepotrebni posli. To so taki, ki so dejansko zaprti, torej obstajajo tudi po peti točki, ampak več kot leto nazaj.
		"""
		# brisanje istosmernih na koncu časovne vrste 

		for key in self.tradeObjectsDict:
			direction = 'S' if key == 'short' else 'B'
			
			oppositeQuantity = sum([trade.quantity 
									for trade in self.tradeObjectsDict[key]
									if trade.buysell != direction])
									# Potrebujemo samo toliko poslov iste smeri, kot imamo zaprtij
			
			directionQuantity = sum([trade.quantity
									for trade in self.tradeObjectsDict[key]
									if trade.buysell == direction])
									# Istosmerni posli

			if oppositeQuantity == 0:
				self.residualTradeObjects.extend([trade for trade in self.tradeObjectsDict[key]])
				self.tradeObjectsDict[key] = []
					
			elif directionQuantity > oppositeQuantity:
				# Če imamo odvečne posle, moramo najti posel, kjer direction quantity dohiti ali prehiti opposite Quantity

				runningdirectionQuantity = 0 # Prvi posel

				for idx,thisTrade in enumerate(self.tradeObjectsDict[key]):
					
					if thisTrade.buysell == direction:
			
						runningdirectionQuantity += thisTrade.quantity

					if runningdirectionQuantity == oppositeQuantity:
						lastTradeIdx = idx
						break
					elif runningdirectionQuantity > oppositeQuantity:
						lastTradeIdx = idx
						self.residualTradeObjects.append(
							self.tradeObjectsDict[key][lastTradeIdx].splitTradeForced(
								runningdirectionQuantity - oppositeQuantity)
								)

						break
					else:
						continue
				
				tradesToMove = [trade for trade in self.tradeObjectsDict[key][1+lastTradeIdx:] if trade.buysell == direction]
				if len(tradesToMove) != 0:
					self.residualTradeObjects.extend(tradesToMove)
					for trade in tradesToMove:
						self.tradeObjectsDict[key].remove(trade)

			for trade in self.residualTradeObjects:
				trade.stock = None

	def removeTradesFromPreviousYears(self, year):
		"""
		Funkcija gre čez reversed seznam pozicij. Ko prvič najde takega, ki ni iz letos in ima zalogo 0, briše ostale.
		Funkcija bi morala nabrati količino zaprtih pozicij pred relevantnim letom
		Potem napredovati čez seznam istosmernih poslov, odstraniti do te količine.
		"""
		# 

		for key in self.tradeObjectsDict:
			direction = 'S' if key == 'short' else 'B'
			
			closed_in_previous_years = sum([trade.quantity 
									for trade in self.tradeObjectsDict[key]
									if trade.buysell != direction
									and str(trade.timestamp.year) < year])
									# Potrebujemo samo toliko poslov iste smeri, kot imamo zaprtij
			
			if closed_in_previous_years != 0:
				# Če imamo zaprte v prejšnjih letih, moramo najti posel, kjer direction quantity dohiti ali prehiti closed_in_previous_years

				running_direction_quantity = 0 # Prvi posel

				trades_to_remove = []

				for idx,thisTrade in enumerate(self.tradeObjectsDict[key]):

					if thisTrade.buysell == direction:
									# istosmerni posel
			
						running_direction_quantity += thisTrade.quantity

						if running_direction_quantity == closed_in_previous_years:
							trades_to_remove.append(thisTrade)
							logger.info('Removing trade {} - it was closed in previous year.'.format(thisTrade))
							break
						elif running_direction_quantity > closed_in_previous_years:
							# posel, ki ga gledamo, je bil delno zaprt v obravnavanem letu. V seznam moramo dodati preostanek
							self.tradeObjectsDict[key].insert(
													idx + 1,
													self.tradeObjectsDict[key][idx].splitTradeForced(
													running_direction_quantity - closed_in_previous_years)
							)

							trades_to_remove.append(thisTrade)
							logger.info('Removing trade {} - it was closed in previous year.'.format(thisTrade))
							break
						else:
							trades_to_remove.append(thisTrade)
							logger.info('Removing trade {} - it was closed in previous year.'.format(thisTrade))
							continue

				# Brišemo zapiranja iz prejšnjega leta

				for trade in self.tradeObjectsDict[key]:
					if trade.buysell != direction:
						if str(trade.timestamp.year) < year:
							trades_to_remove.append(trade)
							logger.info('Removing trade {} - it is a close from previous year.'.format(trade))

				for trade in trades_to_remove:
					self.tradeObjectsDict[key].remove(trade)

	def removeTradesFromPreviousYearsOld(self, year):
		"""
		Funkcija gre čez reversed seznam pozicij. Ko prvič najde takega, ki ni iz letos in ima zalogo 0, briše ostale.
		Funkcija bi morala nabrati količino zaprtih pozicij pred relevantnim letom
		Potem napredovati čez seznam istosmernih poslov, odstraniti do te količine.

		"""
		def get_relevant_trades(list_of_trades, year):

			list_of_trades.reverse()
			relevant_trades = []

			if list_of_trades:
				if str(list_of_trades[0].timestamp.year) != year:
					logger.info('**Whole instrument {} not relevant**'.format(self.ticker))
					return relevant_trades

			for trade in list_of_trades:

				if str(trade.timestamp.year) == year:
					relevant_trades.append(trade)
					logger.info('{}. Trade {} is relevant'.format(self.ticker, trade))

				else:
					if trade.stock != 0:
						relevant_trades.append(trade)
						logger.info('{}. Trade {} is relevant'.format(self.ticker, trade))
					else:
						logger.info('{}. **Trade {} not relevant**'.format(self.ticker, trade))
						logger.info('**No more trades are relevant for {}**'.format(self.ticker))
						break
			
			relevant_trades.reverse()

			return relevant_trades

		self.tradeObjectsDict['short'] = get_relevant_trades(self.tradeObjectsDict['short'], year)

		self.tradeObjectsDict['long'] = get_relevant_trades(self.tradeObjectsDict['long'], year)

	def removeTradesFromFutureYears(self, year):
		"""
		Funkcija odstrani vse posle, ki so za obravnavanim letom
		"""
		filtered_list = []
		for trade in self.tradeObjects:
			if str(trade.timestamp.year) > year:
				logger.info('Trade {} for instrument {} is after {}, removing.'.format(trade, self.ticker, year))
			else:
				filtered_list.append(trade)

		self.tradeObjects = filtered_list

	def finalize_values_for_output(self):
		quantity_multiplier = self.getQuantityMultiplier()
		price_multiplier = self.getPriceMultiplier()
		directions = ['long', 'short']
		for direction in directions:
			for trade in self.tradeObjectsDict[direction]:
				trade.quantity_for_output = round(trade.quantity * quantity_multiplier, 4)
				trade.price_for_output = round(trade.price * price_multiplier, 4)
				trade.stock_for_output = round(trade.stock * quantity_multiplier, 4)
				
	def opposite_trades_exist_within_30_days(self, direction, date):
		"""
		checks all trades in the specified direction and the period +- 30 days
		"""
		logger.debug('Checking for virtual trades for instrument {} for direction {} on {:%Y-%m-%d}.'.format(
										self.name,
										direction,
										date))
		trades_exist = False
		
		clean_date = date.replace(hour = 0, minute = 0, second = 0)
		
		period = (clean_date - datetime.timedelta(days = 30), clean_date + datetime.timedelta(days = 30))
		
		all_trades_in_direction = [trade for trade in self.tradeObjectsDict["long"] if trade.buysell == direction] + [trade for trade in self.tradeObjectsDict["short"] if trade.buysell == direction]
		
		for trade in all_trades_in_direction:
			if trade.timestamp > period[0] and trade.timestamp < period[1]:
				logger.debug("Trade {} {} on {} prevents tax loss claim for the losing trade made on {:%Y-%m-%d}".format(
										direction,
										self.name,
										trade.timestamp,
										clean_date
										))
				trades_exist = True
				
		return trades_exist
		
	def make_profit_loss_pairs(self):
		"""
		Vse posle se da v FIFO pare
		"""
		directions = ['long', 'short']
		
		for direction in directions:
			direction_trade = 'B' if direction == 'long' else 'S'
			position_increases = []
			position_decreases = []
			trade_pairs = []
			
			for trade in self.tradeObjectsDict[direction]:
				if trade.buysell == direction_trade:
					position_increases.append(trade)
					logger.debug('Found trade for position increase: {}, {} {} @ {} on {:%Y-%m-%d} '.format(
											self.name,
											trade.buysell,
											trade.quantity,
											trade.price,
											trade.timestamp))										
				
				else:
					position_decreases.append(trade)
					logger.debug('Found trade for position reduction: {}, {} {} @ {} on {:%Y-%m-%d} '.format(
											self.name,
											trade.buysell,
											trade.quantity,
											trade.price,
											trade.timestamp))										
											
			# iterira se cez vsak position decrease. Tam se vzame dovolj trejdov iz bildanja pozicij, da se matcha kolicin
			
			while position_decreases:
				profit_loss_pair = []
				if position_decreases[0].quantity < position_increases[0].quantity:
					# potrebno razdeliti position_increases[0]
					trade_for_increase = position_increases[0].splitTradeForced(
							position_decreases[0].quantity)
					profit_loss_pair = [trade_for_increase, position_decreases[0]]
					position_decreases.remove(position_decreases[0])
				elif position_decreases[0].quantity == position_increases[0].quantity:
					# razdeli se nic, odstrani se oba
					profit_loss_pair = [position_increases[0], position_decreases[0]]
					position_decreases.remove(position_decreases[0])
					position_increases.remove(position_increases[0])
					
				else:
					# potrebno razdeliti position_decreases[0]
					trade_for_decrease = position_decreases[0].splitTradeForced(
							position_increases[0].quantity)
					profit_loss_pair = [position_increases[0], trade_for_decrease]
					position_increases.remove(position_increases[0])										
					

				self.profit_loss_pairs[direction].append(profit_loss_pair)					
				logger.debug('New profit loss pair. Opened by {} @ {}, closed by {} @ {}'.format(
							profit_loss_pair[0].quantity,
							profit_loss_pair[0].price,
							profit_loss_pair[1].quantity,
							profit_loss_pair[1].price))
				
	def calculate_tax_burden(self):
		"""								
		Pogleda se za vsak par, kjer je bil dobicek:
			apply stroske, daj v pozitiven tax base glede na pravi rate
		Potem se pogleda za vse, kjer je izguba:
			ce so delnice in je odsvojitev navidezna, skip
				ce ni, povecaj negative tax base
				
		Potem se zmanjsuje pozitiven tax base v vseh ratih
				
		"""
		directions = ['long', 'short']
		instrument_group = 'derivatives' if self.type in ('01', '02', '03', '04') else 'stocks'
		trade_costs = 0.0025 if instrument_group == 'derivatives' else 0.01		
		
		for direction in directions:
			
			for pair in self.profit_loss_pairs[direction]:
				pair_outcome = 0
				adjusted_outcome = 0
				pair_outcome = sum([trade.get_signed_value(self.multiplier) for trade in pair])
				holding_period = relativedelta(pair[1].timestamp, pair[0].timestamp).years
				periods = sorted(self.taxable.keys())
				holding_period_tax = periods[0]
				for i, period in enumerate(periods):
					if i == len(periods):
						if holding_period >= period:
							holding_period_tax = -1
					else:
						if holding_period >= period:
							holding_period_tax = periods[i + 1]

				logger.debug('Checking profit/loss for {}. Net outcome for this pair {}.'.format(self.name, pair_outcome))
				
				logger.debug('Holding period is less than {} years'.format(holding_period_tax))
                # ali je bil dejanski dobicek?				
				if pair_outcome > 0:
					for trade in pair:
						if trade.buysell == 'B':
							adjusted_outcome += (1 + trade_costs) * float(trade.get_signed_value(self.multiplier))
						else:
							adjusted_outcome += (1 - trade_costs) * float(trade.get_signed_value(self.multiplier))  
							
					if adjusted_outcome < 0:
						logger.debug('It is negative so actually there is no effect on tax_base.')
					else:
						if holding_period_tax == -1:
							logger.debug('Holding period is more than {} years, no tax base increase.'.format(periods[-1]))
						else:
							self.taxable[holding_period_tax] += adjusted_outcome
							logger.debug('Increasing {} tax base by {}.'.format(instrument_group, adjusted_outcome))

				else: # dejanska izguba
					date_to_check = pair[1].timestamp
					trade_direction = 'B' if pair[1].buysell == 'S' else 'S'
					if instrument_group == 'stocks':
						if not self.opposite_trades_exist_within_30_days(trade_direction, date_to_check):
							logger.debug('Not virtual, reducing tax_base by {}.'.format(pair_outcome))			
							self.unclaimed_loss += float(abs(pair_outcome))
					else:
						logger.debug('Reducing tax_base by {}.'.format(pair_outcome))			
						self.unclaimed_loss += float(abs(pair_outcome))				
	
	def getResidualsAsString(self):
		"""
		Funkcija vrne residuals posle kot string za residuals file
		"""
		outgoingString = ''
		if len(self.residualTradeObjects) != 0:
			for trade in self.residualTradeObjects:
				outgoingString += '{:s},{:s},{:%Y-%m-%d},{:%H:%M:%S},{:.2f},{:.2f},{:s},{:s},{:.2f}\n'.format(self.name,trade.buysell,trade.timestamp,trade.timestamp,trade.quantity,trade.price,self.type,self.currency,self.multiplier)
		
		return outgoingString
				
	def __repr__(self):
		trades = '\n'.join(['\t{}'.format(trade) for trade in self.tradeObjects])
		return "Group: {}, Ticker: {}, Trades:\n{}".format(self.group, self.ticker, trades)
